---
nom: "Syncplay"
date_creation: "jeudi, 28 décembre, 2023 - 18:22"
date_modification: "jeudi, 28 décembre, 2023 - 18:22"
logo:
    src: "images/logo/Syncplay.png"
site_web: "https://syncplay.pl/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Syncplay permet aux gens de profiter d'une expérience de visionnage partagée même s'ils sont à des milliers de kilomètres l'un de l'autre."
createurices: "https://syncplay.pl/about/development/"
alternative_a: ""
licences:
    - "Licence Apache (Apache)"
tags:
    - "vidéo"
    - "synchronisation"
    - "décentralisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Syncplay est un outil qui permet de regarder des anime, des films, des émissions de télévision et d'autres médias ensemble à distance. Ce n'est pas un lecteur vidéo, ni un service de streaming.
Syncplay lance le lecteur vidéo de votre choix (mpv, VLC, MPC-BE, MPC-HC ou IINA), et ouvre le même fichier vidéo chez tous les participants. Syncplay reconnait le fichier avec son nom, ce qui veut dire que chaque participant peut avoir une version différente (qualité, langue, sous-titres...) sans problème. Il intègre aussi un tchat basique (non chiffré) pour les participants, mais il est conseillé de discuter sur un service tierce tel que Mumble.
Syncplay n'est pas conçu pour le streaming (bien que si le lecteur vidéo et la connexion internet le permettent, on peut lire un fichier partagé en ligne ou une vidéo par exemple sur Youtube), et est utilisable même par des personnes avec une connexion internet lente.
Syncplay ne chiffre pas ses communications, donc les noms de fichiers et les messages dans le tchat seraient visibles par une personne malveillante. Il y a la possibilité de hacher les informations partagées, ou de ne pas les partager du tout, trouvable dans les paramètres.
