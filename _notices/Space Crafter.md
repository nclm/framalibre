---
nom: "Space Crafter"
date_creation: "Samedi, 17 février, 2018 - 09:58"
date_modification: "Vendredi, 7 mai, 2021 - 11:14"
logo:
    src: "images/logo/Space Crafter.jpg"
site_web: "http://www.lss-planetariums.info/spacecrafter/index.php?lang=fr&page=home"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Voyager dans l'espace avec un ordinateur puissant"
createurices: "Fabien Chereau"
alternative_a: "Stellarium"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "astronomie"
    - "espace"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Space_Crafter"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/space-crafter"
---

Space Crafter est un logiciel libre de planétarium, sous licence GNU GPL, disponible pour GNU/Linux, Windows et Mac OS X. Il utilise OpenGL pour afficher le ciel de manière photo-réaliste en temps réel. Il est ainsi possible de simuler sur un écran ce que l'on verrait réellement à l'œil nu, aux jumelles ou avec un petit télescope.

