---
nom: "Gitea"
date_creation: "Lundi, 27 mars, 2017 - 09:40"
date_modification: "Mercredi, 7 août, 2019 - 12:23"
logo:
    src: "images/logo/Gitea.png"
site_web: "https://gitea.io/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Gitea est une interface web permettant de visualiser ses projets git"
createurices: ""
alternative_a: "Github"
licences:
    - "Licence MIT/X11"
tags:
    - "git"
    - "gestion de version"
    - "forge logicielle"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gitea"
---

Gitea est une interface web permettant de visualiser les branches, les commits, les tags, etc. de ses projets git. Il a une utilisation proche de celle de GitHub, tout en restant très léger. Il doit être auto-hébergé.

