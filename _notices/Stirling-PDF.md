---
nom: "Stirling-PDF"
date_creation: "jeudi, 6 juin, 2024 - 11:28"
date_modification: "jeudi, 6 juin, 2024 - 11:28"
logo:
    src: "images/logo/Stirling-PDF.png"
site_web: "https://stirlingtools.com/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Outils en ligne de manipulation de fichiers PDF"
createurices: "Anthony Stirling"
alternative_a: "iLovePDF"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "PDF"
    - "manipulation de pdf"
    - "lecteur pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Il s'agit d'un ensemble d'applications permettant d'effectuer différentes opérations sur des fichiers PDF : les couper, les visionner, les réorganiser, les transformer, les convertir, leur donner des mots de passe, extraire leurs différents composants... la liste des tâches possibles est longue. Les fichiers peuvent rester côté client ou peuvent être stockés temporairement sur le serveur pour effectuer certaines opérations qui le nécessitent. En revanche, les applications ne font aucun appel externe et vous ne transmettez pas d'information à un tiers. L'ensemble fonctionne comme un logiciel sur votre ordinateur.
