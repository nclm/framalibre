---
nom: "XAMPP"
date_creation: "Samedi, 7 janvier, 2017 - 19:54"
date_modification: "Vendredi, 21 avril, 2017 - 20:10"
logo:
    src: "images/logo/XAMPP.png"
site_web: "https://www.apachefriends.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "XAMPP est un logiciel qui génère un serveur Apache, MariaDB, PHP, Perl en local."
createurices: "Apache Friends"
alternative_a: "mamp, Windows ISS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "web"
    - "mysql"
    - "mariadb"
    - "php"
lien_wikipedia: "https://fr.wikipedia.org/wiki/XAMPP"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/xampp"
---

XAMPP (X (cross en anglais), Apache, MariaDB, Perl, PHP) est un logiciel qui crée un serveur local pour développer ses sites web dotés d'une base de données par exemple.
Il permet aussi grâce à TomCat d'ajouter des modules comme Joomla, Wordpress, phpmyadmin, OpenSSL, ...

