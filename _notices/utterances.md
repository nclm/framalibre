---
nom: "utterances"
date_creation: "Jeudi, 25 octobre, 2018 - 01:44"
date_modification: "Jeudi, 25 octobre, 2018 - 01:44"
logo:
    src: "images/logo/utterances.png"
site_web: "https://utteranc.es/"
plateformes:
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Un système de commentaires basé sur les tickets Github"
createurices: ""
alternative_a: "disqus"
licences:
    - "Licence MIT/X11"
tags:
    - "cms"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/utterances"
---

Vous rêviez d'une alternative libre à Disqus, vous vous disiez qu'étant donné que votre blog est à destination de développeurs, vous pourriez utiliser les tickets Github pour poster des commentaires ? Le développeur d'Utteranc.es l'a fait.
Au chargement de la page, le code Javascript d'Utterances regarde dans votre dépôt Github s'il y a un ticket correspondant à l'article, selon une méthode à choisir (doit-il chercher le titre de l'article ou toute l'url ?). Les lecteurs et lectrices soit autorisent une application Github à commenter dans la page, soit vont directement commenter dans le ticket associé. Et le tour est joué.

