---
nom: "Communecter"
date_creation: "Mercredi, 12 juillet, 2017 - 00:41"
date_modification: "Lundi, 10 mai, 2021 - 13:31"
logo:
    src: "images/logo/Communecter.png"
site_web: "https://www.communecter.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un réseau social citoyen libre pour construire en commun le futur de nos territoires dès aujourd’hui."
createurices: "Association Open Atlas"
alternative_a: "Facebook"
licences:
    - "Licence Apache (Apache)"
tags:
    - "réseau social"
    - "communication"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/communecter"
---

APPLICATION
TOUS : retrouver les acteurs d'un territoire
ACTUALITÉ : calendrier des actions locales
ÉVÈNEMENTS : actualité localisée
ANNONCES : vendez des biens et des services, proposez et recherchez des ressources humaines ou matérielles
AGORA CITOYENNE : votes et consultations citoyennes
OUTILS
ESPACE COOPÉRATIF : outil de gouvernance pour les projets et les organisations
NETWORK : création de cartes personnalisées
MESSAGERIE : outil de discussions instantanées
GESTIONNAIRE DE FICHIERS ET D'URL : agréger de la connaissance et partage de listes de liens
CUSTOM : créer un réseau personnalisé ré-utilisant toutes les fonctionnalités de la plateforme
FORMULAIRE : récoltez et analysez de l'information par questionnaire
Liste complète : https://doc.co.tools/books/2---utiliser-loutil/page/liste

