---
nom: "Ventoy"
date_creation: "Samedi, 5 septembre, 2020 - 14:26"
date_modification: "Dimanche, 7 février, 2021 - 16:29"
logo:
    src: "images/logo/Ventoy.png"
site_web: "https://www.ventoy.net"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Permet de créer une clé bootable sans aucun formatage."
createurices: "Longpanda"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "utilitaire"
    - "iso"
    - "multiboot"
    - "usb"
    - "live usb"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/ventoy"
---

Il suffit d'installer Ventoy sur la clé et d'ajouter le ou les fichiers ISO à la racine de la clé (par exemple un ISO de Emmabuntüs, un de Fedora, un de Windows 10 et un de Windows 7). Au démarrage, Ventoy propose un menu avec la liste des fichiers ISO présent sur la clé. Il suffit de choisir celui que l'on désire installer et il s’exécute.
Il est compatible avec plus de 550 fichiers ISO (Linux, Windows).

