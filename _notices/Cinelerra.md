---
nom: "Cinelerra"
date_creation: "Dimanche, 28 janvier, 2018 - 19:10"
date_modification: "Vendredi, 7 octobre, 2022 - 00:31"
logo:
    src: "images/logo/Cinelerra.png"
site_web: "http://cinelerra.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un outil de montage audio et vidéo."
createurices: "Adam Williams"
alternative_a: "Adobe Premiere, Sony Vegas Pro, Apple Final Cut, Avid, Lightworks"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "montage vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Cinelerra"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cinelerra"
---

Cinelerra est un outil de montage qui permet de lire, éditer, couper, coller différents médias audio et vidéo, et qui comporte de nombreuses fonctions de montage telles que les transitions vidéos et les effets sonores.
Toutes les opérations de montage vidéo sont en temps réel, il est donc possible de visualiser dans la fenêtre du compositor les effets sans aucun calcul.
Le logiciel se présente sous la forme de quatre palettes de travail :
visualisation et découpage des ''rushes'' vidéos ;
prévisualisation du rendu final ;
montage et timeline ;
ressources, bibliothèques d'effets spéciaux et d'outils divers.
Cinelerra est l'outil open-source le plus proche des standards hollywoodiens FinalCut, PremièrePro, Avid, Sony Vegas...
Trois versions co-existent :
Cinelerra-CV (par la communauté)
Cinelerra-GG (par un contributeur « good guy »)
Cinelerra-HV (par Adam Williams de Heroine Virtual)

