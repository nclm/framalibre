---
nom: "Transmageddon"
date_creation: "Mardi, 20 octobre, 2020 - 11:11"
date_modification: "Lundi, 10 mai, 2021 - 13:29"
logo:
    src: "images/logo/Transmageddon.png"
site_web: "http://www.linuxrising.org/index.html"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Transmageddon est un logiciel de conversion de fichiers vidéos."
createurices: "Christian Schaller"
alternative_a: "VideoProc"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "multimédia"
    - "convertisseur"
    - "vidéo"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/transmageddon"
---

Transmageddon est un convertisseur de fichiers vidéo. À partir d'un fichier d'entrée, on peut choisir le (ou les) format(s) de sortie. Tous les formats habituels sont pris en charge (le logiciel est basé sur la bibliothèque GStreamer).
Il possède une interface simple, sans trop d'options, facile à prendre en main.
Changer de format et de codec permet de diminuer la taille d'une vidéo. Par exemple, à partir d'un fichier .MOV d'un appareil photo à une résolution de 4k, le convertir en MPEG4 permet une réduction drastique de sa taille (jusqu'à dix fois moindre).
Transmageddon est développé par la fondation GNOME.

