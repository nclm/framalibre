---
nom: "Miniflux"
date_creation: "Mercredi, 7 février, 2018 - 20:24"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/Miniflux.png"
site_web: "https://miniflux.net/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Miniflux est un agrégateur de flux RSS entièrement écrit en Go."
createurices: "Frédéric Guillot"
alternative_a: "Feedly, Netvibes, Google Reader"
licences:
    - "Licence Apache (Apache)"
tags:
    - "lecteur de flux rss"
    - "atom"
    - "agrégateur"
    - "auto-hébergement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Miniflux"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/miniflux"
---

Miniflux est un lecteur de flux RSS simple et efficace. L'affichage des articles est optimisé pour la lecture sur écran et le design de l'application est volontairement épuré.
Les fonctions sont simples : l'affichage des flux, leur catégorisation, la gestion de marque-pages et l'envoi des articles vers des services tiers (comme Instapaper, Pinboard et wallabag). L'outil permet l'import et l'export des listes de flux (avec leurs catégories) via un fichier OPML. Soit la base de ce que proposent ces agrégateurs. L'outil est compatible avec les flux Atom, JSON et RSS (1.0 et 2.0).

