---
nom: "CrystalDATA"
date_creation: "Jeudi, 2 novembre, 2023 - 15:11"
date_modification: "mardi, 16 avril, 2024 - 10:33"
logo:
    src: "images/logo/CrystalDATA.png"
site_web: "https://crystal-data.philnoug.com"
plateformes:
    - "le web"
langues:
    - "Français"
description_courte: "Vos données métier 'In da Cloud'"
createurices: "Philippe NOUGAILLON"
alternative_a: "Microsoft Access, Filemaker, Airtable"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "travail collaboratif"
    - "nocode"
    - "base de données"
    - "application web"
    - "formulaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/crystaldata"
---

CrystalData est un gestionnaire d'informations NO-CODE qui vous permet de créer des collections d'objets métiers (ex: Clients, Interventions, Technicien, Frais, Projets, etc.), de les lier (Clients <==> Interventions <==> Techniciens <==> Frais) et de les partager en ligne avec vos collaborateurs.
