---
nom: "BRL-CAD"
date_creation: "Vendredi, 6 mai, 2022 - 18:47"
date_modification: "Vendredi, 6 mai, 2022 - 18:47"
logo:
    src: "images/logo/BRL-CAD.png"
site_web: "https://brlcad.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Comme vous l'avez peut-être déjà deviné, BRL-CAD est un p"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "cad"
    - "cao"
    - "modélisation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/brl-cad"
---

Comme vous l'avez peut-être déjà deviné, BRL-CAD est un puissant système de modélisation solide multiplateforme open source qui comprend l'édition géométrique interactive, le lancer de rayons haute performance pour le rendu et l'analyse géométrique, une suite d'analyse des performances du système, des bibliothèques de géométrie pour développeurs d'applications. Il a plus de 30 ans de développement actif et est considéré comme l'une des meilleures applications pour le développement CAO.
Il est si bon qu'il a été le principal système de CAO de modélisation solide à trois services utilisé par l'armée américaine pour modéliser des systèmes d'armes à des fins d'analyse de vulnérabilité et de létalité. BRL-CAD est soumis aux conditions de licence approuvées par l'OSI. Il est disponible pour Linux, macOS ainsi que Windows.

