---
nom: "ReVanced"
date_creation: "jeudi, 28 décembre, 2023 - 19:28"
date_modification: "jeudi, 28 décembre, 2023 - 19:28"
logo:
    src: "images/logo/ReVanced.png"
site_web: "https://revanced.app/"
plateformes:
    - "Android"
langues:
    - "English"
description_courte: "ReVanced permet d'ajouter, supprimer et modifier des fonctionnalités existantes dans les applications Android."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "android"
    - "utilitaire"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q118315998"
mis_en_avant: "non"

---

ReVanced permet d'ajouter, supprimer et modifier des fonctionnalités existantes dans les applications Android. Il est le successeur de YouTube Vanced, et permet de patcher des applications, notamment YouTube.
Le nom de l'application principale est ReVanced Manager, et elle contient un outil de recherche qui indique quelles applications sont disponibles et quelle version il est conseillé de télécharger.
ReVanced ne fournit pas les apk, mais propose une recherche internet simple pour trouver une apk depuis l'application.
Les patches sont variés, allant du cosmétique à la protection de la vie privée. Il est conseillé d'utiliser les patches par défaut.

Beaucoup de virus circulent sur internet qui se font passer pour ReVanced (le premier résultat sur DuckDuckGo est un site de phishing, et de même pour tous les autres moteurs de recherche), aussi il est fortement déconseillé de télécharger une apk déjà patchée sur internet.
