---
nom: "Salome-Meca"
date_creation: "Jeudi, 11 avril, 2019 - 13:02"
date_modification: "Vendredi, 7 mai, 2021 - 17:06"
logo:
    src: "images/logo/Salome-Meca.png"
site_web: "https://www.code-aster.org/spip.php?article303"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Ce logiciel est une version de Salome avec le solveur mécanique Code_Aster et divers logiciel EDF intégrés."
createurices: "EDF R&D"
alternative_a: "Abacus, ANSYS Mechanical, CAST3M"
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "science"
    - "ingénierie"
    - "calcul scientifique"
    - "mécanique"
    - "cae"
    - "physique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/salome-meca"
---

En plus des fonctionnalités de base de Salome, Salome-meca comporte le solveur mécanique Code_Aster ainsi qu'un module lui donnant une interface graphique et l'intégrant à Salome. Lui permettant d'importer les maillages crée sur Salome et d'ouvrir les fichiers de post-traitements. Permettant et d'avoir toute l'étude dans un seul logiciel.
Divers autre module open source d'EDF sont intégrés comme YACS qui permet de planifier les calculs, Openturns qui permet de faire de la propagation d'incertitudes, ADAO et EFICAS pour retrouver les paramètres à partir des mesures.

