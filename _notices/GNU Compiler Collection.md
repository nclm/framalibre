---
nom: "GNU Compiler Collection"
date_creation: "Mercredi, 23 janvier, 2019 - 20:01"
date_modification: "Lundi, 10 mai, 2021 - 13:18"
logo:
    src: "images/logo/GNU Compiler Collection.png"
site_web: "https://gcc.gnu.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "GCC est un compilateur multi-langage développé par le projet GNU."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "compilateur c"
    - "compilateur c++"
    - "compilation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GNU_Compiler_Collection"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnu-compiler-collection"
---

GCC est un compilateur multi-langage (C, C++, Objective-C, Ada, Fortran, D) et multi-plateformes développé par le projet GNU. Il est en général encore le compilateur par défaut parmi les développeurs Linux.

