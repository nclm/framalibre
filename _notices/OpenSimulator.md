---
nom: "OpenSimulator"
date_creation: "Lundi, 13 novembre, 2017 - 13:40"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/OpenSimulator.jpg"
site_web: "http://opensimulator.org/wiki/Main_Page"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "OpenSimulator est un serveur de monde virtuel 3D, multi-utilisateurs qui permet une utilisation décentralisée."
createurices: "Darren Guard"
alternative_a: "Second Life"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "jeu"
    - "simulation"
    - "3d"
    - "éducation"
    - "monde virtuel"
    - "réseau social"
    - "jeu vidéo"
lien_wikipedia: "https://fr.wikipedia.org/wiki/OpenSimulator"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/opensimulator"
---

OpenSimulator est un serveur de monde virtuel multiplateformes et multi-utilisateurs qui propose de nombreuses fonctionnalités utilisées par exemple pour  mettre en place des plates-formes éducatives, la présentation de travaux augmentés par la 3D, des jeux sérieux, le travail collaboratif à distance. L'accès aux mondes virtuels se fait via un navigateur 3D open source comme Singularity ou Firestorm.
Fonctionnalités :
- utilisation d'avatars 3D,
- messageries instantanées publiques, privées et de groupe avec possibilité d'enregistrer les messages,
- communication orale,
- affichage de tous types de ressource web,
- système de gestion de groupes,
- système de gestion des profils,
- moteur de script,
- partage de documents,
- construction et texturage en direct dans le monde virtuel seul ou à plusieurs,
- diffusion audio et vidéo,
- hypergrid, qui offre un environnement décentralisé similaire à Diaspora* ,
- création, configuration de robots (NPC).

