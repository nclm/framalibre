---
nom: "Otter"
date_creation: "Mercredi, 18 janvier, 2017 - 01:10"
date_modification: "Vendredi, 21 mai, 2021 - 09:54"
logo:
    src: "images/logo/Otter.png"
site_web: "https://otter-browser.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Navigateur Web visant a recréer l'expérience d'opera 12 et à rendre le contrôle du navigateur à l'usager."
createurices: "Michał Dutkiewicz"
alternative_a: "Opera, Google Opera, Microsoft Edge, Apple Safari, Maxthon, Google Chrome"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "navigateur web"
    - "web"
lien_wikipedia: "https://en.wikipedia.org/wiki/Otter_Browser"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/otter"
---

Otter est un projet qui a vu le jour après qu'Opera ait abandonné son navigateur renommé pour migrer vers le moteur de rendu blink de google. Basé sur QT, Otter vise a recréer le meilleur de ce qui a fait le succès d'opera 12.
Bien qu'Otter ne soit pas encore fini et est encore en développement, actuellement en version béta, Il est néanmoins déjà utilisable et propose de nombreuses fonctionnalités: speed dial, bloqueur de contenu, gestionnaire de téléchargements, prise de notes, zoom, gestes à la souris,...
Il est en bonne voie d'être le digne successeur d'opera 12.

