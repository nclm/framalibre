---
nom: "Wire"
date_creation: "Mardi, 25 avril, 2017 - 15:05"
date_modification: "Mercredi, 12 mai, 2021 - 16:19"
logo:
    src: "images/logo/Wire.png"
site_web: "https://wire.com/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
langues:
    - "Autres langues"
description_courte: "Wire est une messagerie instantanée, sécurisé, utilisant le chiffrement de bout en bout pour tous les échanges"
createurices: "Jonathan Christensen, Priidu Zilmer, Alan Duric"
alternative_a: "Skype, WhatsApp, Viber, Google Hangouts, Discord"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "voip"
    - "visioconférence"
    - "messagerie instantanée"
    - "communication chiffrée"
    - "vie privée"
    - "chiffrement"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/wire"
---

Appels en qualité HD, discussions privées en groupe avec partage de photos, musique et vidéos. Sécurisé et parfaitement synchronisé sur vos appareils. C'est un logiciel payant, attention !
Fonctionnalités
Nouvelle recherche
@utilisateur
Message audio + filtres vocaux
Appels vocaux et vidéo
Conversations 1:1 et groupe
Pour plusieurs appareils
Privés
Pas de pubs
Pas de profil ni de collecte de données
Aucun partage de contact requis
Aucun numéro de téléphone requis
Inscription par courriel disponible
Sécurisé
Chiffrement complet de bout en bout
Open source
Audité indépendante
Code dans GitHub
Basé en Suisse, hébergé en zone UE
Protégé par les lois européennes

