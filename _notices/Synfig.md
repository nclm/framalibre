---
nom: "Synfig"
date_creation: "Vendredi, 10 mars, 2017 - 19:05"
date_modification: "Vendredi, 10 mars, 2017 - 19:05"
logo:
    src: "images/logo/Synfig.png"
site_web: "http://www.synfig.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Logiciel professionnel d'animation en 2D"
createurices: "Robert Quattlebaum"
alternative_a: "MonkeyJam, Adobe Animate CC, Prezi, stopmotion, Flash (animation)"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "animation"
    - "dessin animé"
    - "stopmotion"
    - "2d"
    - "dessin vectoriel"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Synfig"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/synfig"
---

Syncfig est un logiciel de dessin vectoriel permettant de réaliser des animations. D'abord développé exclusivement au sein d'une entreprise, il est devenu logiciel libre en 2005. Il gère des calques de différents types, l'animation par le squelette, l'ajout de son, … Des projets de films libres l'utilisent, comme le projet Morevna (qui liste également les autres logiciels utilisés et fournissent de la documentation et le matériel de leurs films).

