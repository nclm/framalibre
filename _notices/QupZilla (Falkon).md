---
nom: "QupZilla (Falkon)"
date_creation: "Dimanche, 22 février, 2015 - 11:52"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/QupZilla (Falkon).png"
site_web: "http://www.qupzilla.com/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un navigateur Web libre léger et multiplate-forme."
createurices: "David Rosca"
alternative_a: "Internet Explorer, Google Chrome, Opera"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "navigateur web"
    - "web"
lien_wikipedia: "http://fr.wikipedia.org/wiki/QupZilla"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qupzilla"
---

QupZilla est un navigateur web destiné au grand public. Il a pour vocation une intégration poussée aux environnements de bureau des utilisateurs mais propose également divers thèmes. QupZilla est distribué sous licence GPLv3. Il est développé en partant de l'idée que « léger » ne veut pas forcément dire « manque de fonctionnalités ».
Depuis le mois de mars, "Qupzilla" change de nom pour "Falkon"

