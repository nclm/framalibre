---
nom: "pdfimpose"
date_creation: "mardi, 5 mars, 2024 - 19:55"
date_modification: "mardi, 5 mars, 2024 - 19:55"
logo:
    src: "images/logo/pdfimpose.png"
site_web: "https://framagit.org/spalax/pdfimpose"
plateformes:
    - "le web"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Imposez vos documents PDF pour les imprimer et en faire des livres."
createurices: "Louis Paternault"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "manipulation de pdf"
    - "imposition"
    - "reliure"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Le logiciel **pdfimpose** permet [d'imposer](https://fr.wikipedia.org/wiki/Imposition_(imprimerie)) des documents PDF, c'est-à-dire de placer sur une grande feuille les pages d'un ouvrage afin d'obtenir un livre après impression, pliage, et reliure.

Il permet de réaliser différentes dispositions : reliure à la française, magazine, fanzine…

Une version web (par le même auteur, sous la même licence) est disponible : https://pdfimpose.it.
