---
nom: "Inkscape"
date_creation: "Mardi, 3 février, 2015 - 16:37"
date_modification: "Mardi, 19 mai, 2020 - 15:42"
logo:
    src: "images/logo/Inkscape.png"
site_web: "https://inkscape.org/fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Un puissant logiciel de dessin vectoriel."
createurices: ""
alternative_a: "Adobe Illustrator, Vectr"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "dessin"
    - "graphisme"
    - "dessin vectoriel"
    - "svg"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Inkscape"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/inkscape"
---

Inkscape permet d'éditer les fichiers standards SVG, d'exporter du PNG, PDF, etc. Il permet également l'édition de PDF. La plupart des fonctionnalités en dessin vectoriel sont disponibles ce qui permet une prise en main assez rapide pour les débutants. On peut ajouter à cela des fonctionnalités plus intéressantes (et plus techniques) comme l'éditeur XML intégré, la possibilité d'ajouter des plugins en langage Python, et même d'exporter pour LaTeX.

