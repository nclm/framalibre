---
nom: "GNOME Games"
date_creation: "Lundi, 3 septembre, 2018 - 12:46"
date_modification: "Lundi, 10 mai, 2021 - 14:21"
logo:
    src: "images/logo/GNOME Games.png"
site_web: "https://wiki.gnome.org/Apps/Games"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Tous vos jeux vidéo réunis au même endroit"
createurices: "Adrien Plazas"
alternative_a: "Steam, RomStation"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "gnome"
    - "émulateur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gnome-games"
---

Games est un gestionnaire de jeux vidéo intégré au bureau GNOME. Il permet d'afficher toute la ludothèque présente sur l'ordinateur, de la classer par développeur ou plateforme d'origine, de récupérer automatiquement les informations de chaque jeu, et d'émuler une sélection de consoles vidéo pour lancer le maximum de jeux avec le minimum de configuration.

