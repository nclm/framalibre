---
nom: "yt-dlp"
date_creation: "jeudi, 28 décembre, 2023 - 17:40"
date_modification: "jeudi, 28 décembre, 2023 - 17:40"
logo:
    src: "images/logo/yt-dlp.svg"
site_web: "https://github.com/yt-dlp/yt-dlp/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "English"
description_courte: "Télécharger les vidéos de Youtube et de nombreux autres sites"
createurices: ""
alternative_a: "youtube-dl"
licences:
    - "Unlicense"
tags:
    - "internet"
    - "téléchargement de youtube"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Youtube-dl"
lien_exodus: ""
identifiant_wikidata: "Q108454371"
mis_en_avant: "non"

---

Youtube-dl est un logiciel en ligne de commande qui permet de télécharger les vidéos de Youtube et de nombreux autres sites. Il est capable de télécharger les sous-titres ou des playlistes et possède de nombreuses options. Pour l’installer, l’utiliser et le mettre à jour (ce qui est nécessaire de faire régulièrement, pour suivre les changements internes des plateformes), il faut, par défaut, savoir taper des commandes dans un terminal. Mais de nombreuses interfaces graphiques (GUI) plus ou moins officielles et plus ou moins complètes existent. Nous citerons media-downloader (propose des paquets .deb), you2ber, une extension pour Gnome Shell, Tartube (propose des paquets .deb) ou encore youtube-dl-gui (Electron et AppImage).

yt-dlp est la suite spirituelle de Youtube-dl. Contrairement à son prédécesseur, celui-ci est activement mis à jour et incorpore des nouveautés telles que SponsorBlock.
