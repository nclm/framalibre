---
nom: "Mumble"
date_creation: "Mardi, 3 janvier, 2017 - 20:47"
date_modification: "Mercredi, 12 mai, 2021 - 16:52"
logo:
    src: "images/logo/Mumble.png"
site_web: "https://wiki.mumble.info/wiki/Main_Page"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "La Voix sur ip libre et décentralisée."
createurices: "Thorvald Natvig"
alternative_a: "Teamspeak, Discord"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "internet"
    - "voip"
    - "chat"
    - "communication"
    - "social"
    - "jeu"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Mumble"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/mumble"
---

Mumble est un logiciel de voix sur ip assez spartiate dans son interface mais efficace et stable. C'est un logiciel de Voip qui fait bénéficier à ses utilisateurs d'une latence très faible tout en fournissant une qualité de son excellente. Mumble intègre également un "overlay" surcouche visuelle qui se positionne discrètement au dessus des fenêtres de jeux afin d'avoir une vision sur l'activation des micros des gens avec qui vous parlez.
Le logiciel serveur s’appelle Murmur.

