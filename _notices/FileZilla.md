---
nom: "FileZilla"
date_creation: "Mercredi, 11 février, 2015 - 00:07"
date_modification: "lundi, 18 mars, 2024 - 12:25"
logo:
    src: "images/logo/FileZilla.png"
site_web: "https://filezilla-project.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "FileZilla est un client FTP, FTPS et sFTP populaire."
createurices: "Filezilla Project"
alternative_a: "Cerberus, Transmettre 5, WS_FTP, CyberDuck, Commandant un, tectia"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "client ftp"
    - "ftp"
    - "transfert de fichiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FileZilla"
lien_exodus: ""
identifiant_wikidata: "Q300662"
mis_en_avant: "oui"
redirect_from: "/content/filezilla"
---

FileZilla est un client FTP, FTPS et sFTP populaire. Il permet de se connecter en FTP sur les serveurs auxquels on a accès pour y télécharger ou y téléverser des fichiers.
Il gère le stockage des données de connexion de différents serveurs, le transfert de gros fichiers, les connexions multiples, les files d'attente, les limites de bandes passantes, etc.

Attention tout de même pour les utilisteurs de Microsoft Windows : la [version MS Windows possède des spywares](https://sebsauvage.net/links/?weVc7Q). Sous MS Windows il est conseillé d'utiliser [WinSCP](https://framalibre.org/notices/winscp.html). Par contre la version GNU/Linux intégrée aux distributions est clean, c'est uniquement la version MS Windows qui est problématique avec son installeur.
