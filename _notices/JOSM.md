---
nom: "JOSM"
date_creation: "jeudi, 11 avril, 2024 - 09:45"
date_modification: "mardi, 16 avril, 2024 - 14:29"
logo:
    src: "images/logo/JOSM.png"
site_web: "https://josm.openstreetmap.de/wiki/Fr%3AWikiStart"
plateformes:
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "JOSM (éditeur Java pour OpenStreetMap) est une application de bureau destinée à modifier les cartes OSM (OpenStreetMap)."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cartographie"
    - "carte géographique"
    - "sig"
    - "osm"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Java_OpenStreetMap_Editor"
lien_exodus: ""
identifiant_wikidata: "Q12877"
mis_en_avant: "non"

---

Lorsqu'on commence à contribuer à **OpenStreetMap**, le plus intuitif est d'utiliser les fonctionnalités prévues dans le navigateur, dans l'éditeur de carte qui s'appelle ID.

Celui-ci rend de grands services, mais dès qu'on a acquis un peu d'expérience, il est extrêmement profitable de prendre un peu de temps pour se familiariser avec JOSM. Ce temps d'apprentissage sera récupéré cent fois, du fait de l'efficacité de JOSM.
