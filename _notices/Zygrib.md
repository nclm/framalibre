---
nom: "Zygrib"
date_creation: "Jeudi, 5 mars, 2015 - 22:37"
date_modification: "Jeudi, 13 septembre, 2018 - 11:38"
logo:
    src: "images/logo/Zygrib.jpg"
site_web: "http://www.zygrib.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Zygrib permet le téléchargement et l'affichage des données méteo."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "cartographie"
    - "sig"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/zygrib"
---

Zygrib permet le téléchargement et l'affichage des données méteo via le format GRIB.
Le projet semble abandonné depuis le 11/09/2016.
Un fork a vu le jour depuis : XyGrib (https://framalibre.org/content/xygrib)
Fonctionnalités :
Téléchargement automatique de fichiers GRIB,
Téléchargement automatique de fichiers IAC (fleetcode),
Affichage direct de fichiers GRIB bruts ou compressés au format .gz ou .bz2.
Sources de données :
Données cartographiques mondiales GSHHS (précision moyenne ≈ 100 m) pour les fonds de cartes (cartes modifiées par Rainer Feistel pour un affichage plus rapide RANGS),
Données météo en provenance directe du NOAA (modèle GFS),
Données de Meteoblue  (NMM : Numerical Mesoscale Models).
Données de vagues en provenance du FNMOC

