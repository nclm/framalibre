---
nom: "Leed"
date_creation: "Mercredi, 25 janvier, 2017 - 13:50"
date_modification: "dimanche, 7 janvier, 2024 - 18:19"
logo:
    src: "images/logo/Leed.svg"
site_web: "https://github.com/LeedRSS/Leed"
plateformes:
    - "le web"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Leed (contraction de Light Feed) est un agrégateur RSS/ATOM minimaliste qui permet la consultation de flux RSS"
createurices: "Valentin CARRUESCO, Christophe HENRY, Simon ALBERNY, Maël ILLOUZ"
alternative_a: "Netvibes, Feedly, Google Reader"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "lecteur de flux rss"
    - "atom"
    - "agrégateur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/leed"
---

Leed (contraction de Light Feed) est un agrégateur RSS/ATOM minimaliste qui permet la consultation de flux RSS de manière rapide et non intrusive.
Cet agrégateur peut s'installer sur votre propre serveur et fonctionne avec un système de tâches cron afin de traiter les informations de manière transparente et de les afficher le plus rapidement possible lorsque vous vous y connectez.


