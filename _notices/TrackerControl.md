---
nom: "TrackerControl"
date_creation: "jeudi, 28 décembre, 2023 - 20:32"
date_modification: "jeudi, 28 décembre, 2023 - 20:32"
logo:
    src: "images/logo/TrackerControl.png"
site_web: "https://trackercontrol.org/"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "TrackerControl est une application Android qui permet aux utilisateurices de surveiller et de contrôler la collecte de données dans les applications mobiles."
createurices: "Konrad Kollnig (Université d'Oxford)"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "réseau"
    - "bloqueur de publicité"
    - "vie privée"
    - "éducation populaire"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/net.kollnig.missioncontrol.fdroid/latest/"
identifiant_wikidata: ""
mis_en_avant: "non"

---

TrackerControl est une application Android qui permet aux utilisateurices de surveiller et de contrôler la collecte de données dans les applications mobiles. Elle est aussi un projet de recherche de l'Université d'Oxford.
Comme beaucoup d'applications similaires, TrackerControl crée un VPN local pour analyser le réseau, et utilise des listes pour ranger les traceurs dans des catégories.
L'application vise également à éduquer sur vos droits en vertu de la loi sur la protection des données, tels que le règlement général sur la protection des données (RGPD) de l'UE.
