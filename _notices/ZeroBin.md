---
nom: "ZeroBin"
date_creation: "Vendredi, 27 février, 2015 - 13:51"
date_modification: "Mercredi, 18 janvier, 2017 - 01:15"
logo:
    src: "images/logo/ZeroBin.jpg"
site_web: "http://sebsauvage.net/wiki/doku.php?id=php%3Azerobin"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "English"
description_courte: "ZeroBin est un pastbin sous licence libre. Il permet de partager du texte en ligne."
createurices: "Sebsauvage"
alternative_a: "Pastebin"
licences:
    - "Open Software License (OSL)"
tags:
    - "pastebin"
    - "board"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/zerobin"
---

ZeroBin est un pastebin sous licence zlib/libpng OSI développé par Sebsauvage.
ZeroBin permet de partager du texte en ligne, et par sa fonction de commentaire intégré se transforme en board de discussion.
ZeroBin gère le chiffrage en 256 bits AES, offre des options de sécurité tel que l'expiration des données ou le "burn after reading", la coloration syntaxique ou encore la prise en compte automatique des URL.
ZeroBin ne nécessite pas de base de donnée.
Spécifications :
Serveur: php 5.2.6 ou supérieur ; Bibliothèque GD.
Client: un navigateur moderne gérant le javascript.
à noter zerobin n'est plus développé par son auteur, le projet a été repris pour continuer le développement sous le nom privatebin, voir http://sebsauvage.net/links/?ffS82g

