---
nom: "Dolomon"
date_creation: "Dimanche, 28 octobre, 2018 - 17:28"
date_modification: "Dimanche, 28 octobre, 2018 - 17:34"
logo:
    src: "images/logo/Dolomon.png"
site_web: "https://dolomon.org/"
plateformes:
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un outil simple pour compter le nombre de visites sur votre site web !"
createurices: "Luc Didry"
alternative_a: "Google Analytics"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "statistiques"
    - "mesure de statistiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/dolomon"
---

Dolomon (pour DOwnLOad MONitor) est un outil simple pour compter le nombre de visites sur un site web. Il ne traque pas la navigation et se contente de compter anonymement le traffic.
Le logiciel Dolomon est notamment hébergé par Framasoft. Vous pouvez l'utiliser via son service Framaclic. De la documentation est mise à disposition pour faciliter la prise en main de l'outil.
Si votre site est propulsé par Wordpress, le plugin WP-Dolomon permet d'utiliser Dolomon directement.

