---
nom: "NoScript"
date_creation: "mercredi, 21 février, 2024 - 16:25"
date_modification: "mercredi, 21 février, 2024 - 16:25"
logo:
    src: "images/logo/NoScript.png"
site_web: "https://noscript.net/"
plateformes:
    - "le web"
langues:
    - "English"
description_courte: "NoScript Security Suite est un logiciel libre qui offre une protection supplémentaire pour Firefox (sur Android également !), Chrome, Edge, Brave et d'autres navigateurs web."
createurices: "Giorgio Maone"
alternative_a: ""
licences:
    - "Creative Commons (CC-By-Sa)"
tags:

lien_wikipedia: "https://fr.wikipedia.org/wiki/NoScript"
lien_exodus: ""
identifiant_wikidata: "Q476604"
mis_en_avant: "non"

---

NoScript est un composant de sécurité clé intégré au Tor Browser, le meilleur outil d'anonymat qui défend des millions de personnes contre la surveillance et la censure.

Cette extension de navigateur permet à JavaScript et à d'autres contenus potentiellement dangereux de n'être exécutés que par des sites web de confiance de votre choix (par exemple, votre banque en ligne).

NoScript fournit également la protection anti-XSS la plus puissante jamais disponible dans un navigateur.

L'approche unique de blocage préemptif des scripts de NoScript empêche l'exploitation des vulnérabilités de sécurité (connues, telles que Meltdown ou Spectre, et même non encore connues !) sans perte de fonctionnalité : vous pouvez activer JavaScript et d'autres capacités dynamiques pour les sites auxquels vous faites confiance d'un simple clic.
