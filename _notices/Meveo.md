---
nom: "Meveo"
date_creation: "Mardi, 18 octobre, 2022 - 08:25"
date_modification: "Mardi, 18 octobre, 2022 - 08:25"
logo:
    src: "images/logo/Meveo.png"
site_web: "https://meveo.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Editeur WYSIWYG low-code de vos applications backend et frontend."
createurices: ""
alternative_a: "AWS, Zoho, WinDev"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "développement"
    - "backend"
    - "frontend"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/meveo"
---

Meveo est une web-application cloud ready et scalable qui permet de créer de facon dynamique le backend et frontend d'applications web ou web3.

Création de modèle de donnees sur multi datastore : RDBMS, base de graphe, cache distribue, moteur de recherche
Création de fonctions  en java, javascript, python
Création d'API (rest, websocket)
Création de workflow
Création de notifications

L'approche semantique de meveo permet de faire abstraction du type de datastore utilise et d'automatiser les tests et la documentation.
La notion de module basee sur un repository git permet de gerer de maniere performante les mises a jour de modules.
Gestion fine et automatisee de la securite.
Base sur : Wildfly, Graal, Keycloack, Infinispan, Postgres, Neo4j, ElasticSearch, Liquibase.

