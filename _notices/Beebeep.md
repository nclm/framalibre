---
nom: "Beebeep"
date_creation: "Lundi, 19 novembre, 2018 - 15:11"
date_modification: "Mercredi, 12 mai, 2021 - 16:10"
logo:
    src: "images/logo/Beebeep.png"
site_web: "http://beebeep.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Un logiciel de messagerie instantanée en réseau local."
createurices: "Marco Mastroddi"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "chat"
    - "messagerie instantanée"
    - "transfert de fichiers"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/beebeep"
---

BeeBEEP a été développé par Marco Mastroddi pour son travail et il l'a mis à disposition de tous.
Vous pouvez communiquer et partager des fichiers avec toutes les personnes de votre réseau local, par exemple au bureau, au domicile ou dans un cybercafé.
Vous n'avez pas besoin d'un serveur, il suffit de télécharger, décompresser et le démarrer. Simple et rapide.

