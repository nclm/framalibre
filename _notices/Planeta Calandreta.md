---
nom: "Planeta Calandreta"
date_creation: "dimanche, 21 janvier, 2024 - 17:04"
date_modification: "dimanche, 21 janvier, 2024 - 17:04"


site_web: "https://framagit.org/calandreta/planeta"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel web de gestion d'écoles associatives"
createurices: "Christophe Javouhey"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "calandreta"
    - "école associative"
    - "gestion"
    - "cantine"
    - "garderie"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Planeta Calandreta est un outil de gestion développé par la Calandreta de Muret, école associative bilingue occitane, située au sud de Toulouse. Cet outil répond à ses besoins de gérer les inscriptions des enfants à la cantine, à la garderie, de pouvoir suivre la facturation des familles et l'encaissement des paiements... Le module "Coopération" permet le suivi de l'organisation d'événements festifs ou de travaux. Il possède également une gestion des dons et des membres de commissions.
Le module "Forum" permet aux membre de l'association d'échanger entre eux.
Planeta se présente comme une interface web, développée en PHP.
