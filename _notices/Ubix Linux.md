---
nom: "Ubix Linux"
date_creation: "jeudi, 29 février, 2024 - 20:25"
date_modification: "jeudi, 29 février, 2024 - 20:25"
logo:
    src: "images/logo/Ubix Linux.svg"
site_web: "https://ubix-linux.sourceforge.io/en"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Distribution Linux open-source dérivée de Debian conçue pour faciliter l'acquisition, la transformation, l'analyse et la présentation des données."
createurices: "François Gamba"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "distribution gnu/linux"
    - "traitement de données"
    - "analyse statistique"
    - "base de données"
    - "reporting"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q124703162"
mis_en_avant: "non"

---

Le but d'Ubix Linux est d'offrir un datalab prêt à l'emploi, agile et polyvalent. 

Ubix Linux regroupe tous les outils nécessaires pour apprendre les bases de l'analyse de données et de l'intelligence artificielle sur des ensembles de données de petite et de moyenne taille.

Ubix Linux intègre uniquement des solutions libres et ouvertes et peut être utilisé sur de modestes configurations matérielles.
