---
nom: "TestDisk"
date_creation: "Mardi, 5 janvier, 2021 - 10:58"
date_modification: "Mardi, 5 janvier, 2021 - 11:07"
logo:
    src: "images/logo/TestDisk.png"
site_web: "https://www.cgsecurity.org/wiki/TestDisk"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "TestDisk permet la restauration de données et de partitions sur disque dur, SSD, clé USB, etc."
createurices: "Christophe Grenier"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "restauration"
lien_wikipedia: "https://fr.wikipedia.org/wiki/TestDisk"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/testdisk"
---

Pour détecter les éventuelles erreurs dans la table des partitions, TestDisk vérifie la structure du disque pour le réparer :
Récupération de partition effacée
Reconstruction de la table des partitions
Réécriture du Master boot record (MBR)
Partition passée en RAW
Il est également possible de récupérer des fichiers qui ont été supprimés. Pour ça TestDisk contient le logiciel PhotoRec. Contrairement à son nom, il ne récupère pas que des fichiers images mais aussi des fichiers archives, bureautique, multimédia, etc. (au total il couvre plus de 180 extensions).

