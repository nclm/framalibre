---
nom: "QualCoder"
date_creation: "Mercredi, 25 décembre, 2019 - 19:16"
date_modification: "Mercredi, 12 mai, 2021 - 15:13"
logo:
    src: "images/logo/QualCoder.png"
site_web: "https://github.com/ccbogel/QualCoder"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Logiciel de Computer-assisted qualitative data analysis software (CAQDAS)"
createurices: ""
alternative_a: "MaxQDA, Atlas.ti, NVivo"
licences:
    - "Licence MIT/X11"
tags:
    - "science"
    - "sciences humaines et sociales"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/qualcoder"
---

Logiciel d’aide d’analyse de données qualitatives. Utilise la méthode de la théorie ancrée. Permet de sélectionner des morceaux de textes, l’annote et créer des typologies. Possibilité d’export dans de nombreux formats.

