---
nom: "CédiSCO"
date_creation: "lundi, 15 avril, 2024 - 16:33"
date_modification: "lundi, 15 avril, 2024 - 16:33"
logo:
    src: "images/logo/CédiSCO.png"
site_web: "https://openacademie.fr/cedisco"
plateformes:
    - "Windows"
langues:
    - "Français"
description_courte: "CédiSCO pacifie la procédure du conseil de discipline de votre établissement scolaire"
createurices: "OpenAcadémie, startup d'Etat de l'Education nationale"
alternative_a: ""
licences:
    - "Licence Publique Union Européenne (EUPL)"
tags:
    - "Conseils de discipline"
    - "Procédure"
    - "Procès-verbal"
    - "sanctions"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

La procédure des conseils de discipline dans un établissement d'enseignement est exigeante, chronophage, et une erreur est si vite arrivée ! Avec CédiSCO, les convocations sont générées automatiquement, le président du conseil de discipline dispose d'un conducteur de séance pour ne commettre aucun impair, et une aide à la saisie du compte-rendu en temps réel permet d'avoir un PV correct dans les minutes immédiatement après la séance, ce qui favorise une rescolarisation rapide de l'élève en cas d'exclusion.
