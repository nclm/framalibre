---
nom: "TCC"
date_creation: "Vendredi, 24 novembre, 2017 - 05:20"
date_modification: "Lundi, 10 mai, 2021 - 13:18"
logo:
    src: "images/logo/TCC.png"
site_web: "https://bellard.org/tcc/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "English"
description_courte: "Un vrai compilateur C dans un mouchoir de poche"
createurices: "Fabrice Bellard et al."
alternative_a: ""
licences:
    - "Licence publique générale limitée GNU (LGPL)"
tags:
    - "développement"
    - "compilateur c"
    - "compilation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tiny_C_Compiler"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tcc"
---

TCC est un compilateur pour le langage C, mais, contrairement à son grand frère GCC, TCC a un tout petit code source, prend très peu de place et, cerise sur le gâteau, compile à une vitesse phénoménale. Encore mieux : il peut être incorporé, sous forme de bibliothèque (tcclib) dans une application, transformant le langage C en un langage de script qui peut être compilé à la volée.
Ce poids plume se traduit par des compromis : exécutables moins optimisés qu’avec GCC ou clang, messages d’erreur moins explicites, certaines extensions du langage C non prises en charge (les projets qui les utilisent ne compilent pas)… et les dernières versions que nous avons testées plantaient parfois sur GNU/Linux 64 bits (aucun problème en revanche sur Linux 32 bits et Windows).
Malgré ça, tcc évolue encore et sa facilité déconcertante d’installation ainsi que la tcclib en font un outil génial et incomparable, digne du grand Fabrice Bellard (qui ne travaille hélas plus dessus).

