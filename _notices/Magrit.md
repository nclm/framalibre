---
nom: "Magrit"
date_creation: "Jeudi, 20 avril, 2017 - 10:42"
date_modification: "Mercredi, 12 mai, 2021 - 15:57"
logo:
    src: "images/logo/Magrit.png"
site_web: "http://magrit.cnrs.fr/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
description_courte: "Magrit est un logiciel de cartographie thématique ou statistique en ligne."
createurices: ""
alternative_a: "Philcarto, khartis"
licences:
    - "Licence CECILL (Inria)"
tags:
    - "science"
    - "cartographie"
    - "carte géographique"
    - "statistique"
    - "analyse statistique"
    - "statistiques"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/magrit"
---

Magrit est un logiciel de cartographie thématique ou statistique en ligne.
Après avoir importé ses propres données (fichiers csv, shapefile ou geojson par exemple) l'utilisateur peut réaliser un grand nombre de types de cartes telles que les cartes en figurés proportionnels, les cartes choroplèthes ou les cartes de données qualitatives.
Magrit permet aussi de créer d'autres cartes plus complexes utilisant des concepts d'analyse spatiale (cartes lissées,  cartes des discontinuités, carroyages, anamorphoses...).
Une fois mise en page, une carte peut être sauvegardée sous forme de fichier projet ou exportée en png ou en svg.

