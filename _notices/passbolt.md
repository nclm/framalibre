---
nom: "passbolt"
date_creation: "Vendredi, 30 décembre, 2016 - 07:44"
date_modification: "Mercredi, 12 mai, 2021 - 15:44"
logo:
    src: "images/logo/passbolt.png"
site_web: "https://www.passbolt.com"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Passbolt est un gestionnaire de mots de passe conçu pour la collaboration en équipe!"
createurices: "cedric alfonsi, kevin muller, remy bertot"
alternative_a: "lastpass, dashlane, zoho vault"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "sécurité"
    - "mot de passe"
    - "gestionnaire de mots de passe"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/passbolt"
---

Passbolt est un gestionnaire de mots de passe libre conçu pour la coopération. Il permet aux membres d’une équipe de stocker et partager leurs mots de passe de manière sécurisée, et d’être intégré à un écosystème existant par l’intermédiaire de son API et de son client console. Le chiffrement des mots de passe se base sur un standard reconnu: OpenPGP.

