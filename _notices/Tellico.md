---
nom: "Tellico"
date_creation: "Jeudi, 6 février, 2020 - 15:20"
date_modification: "Vendredi, 5 août, 2022 - 13:31"
logo:
    src: "images/logo/Tellico.png"
site_web: "https://tellico-project.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "English"
description_courte: "Un gestionnaire personnel de livres, BDs, vidéos, musique ou vins."
createurices: "Robby Stephenson"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "livre"
    - "collection"
    - "catalogue"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tellico"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tellico"
---

Tellico est un logiciel de bureautique qui permet de rechercher, sauvegarder et gérer ses collections de livres, bandes-dessinées, films, musique, et même de vins. Il permet de chercher les données bibliographiques en ligne, par exemple sur bedetheque.com ou, ce qui marche le mieux pour les livres français, même récents, et incluant même l'image de couverture, Google Book Search.
Il permet d'exporter ses collections dans plusieurs formats, de faire des recherches croisées dans sa collection, d'ajouter quelques informations personnelles aux livres (note, commentaire, résumé…).
Il ne permet pas à notre connaissance de vendre des livres ou de gérer des emprunts. Ce n'est donc pas un logiciel de librairie ou de bibliothèque, mais un gestionnaire de collections personnelles.

