---
nom: "Remix"
date_creation: "dimanche, 31 mars, 2024 - 09:02"
date_modification: "dimanche, 31 mars, 2024 - 09:02"
logo:
    src: "images/logo/Remix.svg"
site_web: "https://educajou.forge.apps.education.fr/remix/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Générer des groupes hétérogènes équilibrés. Par exemple, à partir d'une classe de CP, de CE1 et de CE2, on peut obtenir 5 groupes contenant des élèves de chaque niveau."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "répartition"
    - "école"
    - "groupes"
    - "tableur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Remix est une application permettant de former des groupes d'élèves hétérogènes. Par exemple, à partir d'une classe de CP, de CE1 et de CE2, on peut obtenir 5 groupes contenant des élèves de chaque niveau.

Ce principe peut évidemment s'appliquer à d'autres entités que des classes et des élèves, de façon générale Remix permet de ventiler des données.

Pour commencer, saisssez vos groupes de départ ou importez-les depuis un tableur, puis cliquez sur "Ventiler".

Une fois les groupes ventilés, on peut obtenir une nouvelle répartition aléatoire en cliquant à nouveau sur le bouton "ventiler" (mais toujours dans les mêmes proportions).

Les noms des groupes en sortie (Groupe 1, Groupe 2 ...) sont également modifiables.

Les groupes ainsi obtenus sont exportables en CSV, qu'on peut ouvrir avec un tableur comme LibreOffice Calc ou Excel, par exemple.
