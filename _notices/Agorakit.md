---
nom: "Agorakit"
date_creation: "Lundi, 21 août, 2017 - 11:22"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/Agorakit.PNG"
site_web: "https://www.agorakit.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Groupware pour initiative citoyenne"
createurices: "Philippe Jadin"
alternative_a: "Facebook, Google calendar, Google groups, Dropbox"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "groupware"
    - "forum de discussion"
    - "agenda"
    - "cartographie"
    - "partage de fichiers"
    - "travail collaboratif"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/agorakit"
---

Agorakit est un groupware (ou collecticiel) en ligne, à destination des initiatives citoyennes. En créant des groupes collaboratifs, les utilisateurs peuvent discuter de sujets, organiser des événements, conserver des fichiers et garder tout le monde au courant en fonction des besoins et implications de chacun.
Agorakit est un forum, un calendrier, un gestionnaire de fichier et un outil de notification par mail.
Une cartographie des groupes, personnes et événements est également proposée.
Le tout est organisé sous forme de groupe privés ou publics, autogérés un maximum.
Agorakit est utilisé par plusieurs initiatives citoyennes la plus large installation ayant plus de 1000 utilisateurs.
Agorakit est développé avec le framework Laravel (php) ce qui garantit une organisation moderne du code, une grande vitesse d'exécution et une bonne implémentation de la sécurité.
Plus d'informations (en français) ici https://www.agorakit.org/fr
Une démo sur https://demo.agorakit.org

