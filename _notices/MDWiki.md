---
nom: "MDWiki"
date_creation: "Mercredi, 25 février, 2015 - 18:40"
date_modification: "Lundi, 3 juillet, 2023 - 00:29"
logo:
    src: "images/logo/MDWiki.png"
site_web: "http://mdwiki.info/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "MDwiki est un CMS/Wiki entièrement basé sur HTML5/Javascript, avec la syntaxe markdown."
createurices: "Timo Dörr"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "cms"
    - "wiki"
    - "markdown"
    - "html5"
    - "javascript"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mdwiki"
---

MDwiki est un CMS/Wiki entièrement basé sur HTML5/Javascript, avec la syntaxe markdown. Il fonctionne sur tous navigateurs. Aucune installation de logiciel ou serveur n'est nécessaire. Il suffit simplement de charger le fichier mdwiki.html fourni avec MDwiki dans le même répertoire que les pages en markdown de votre site et de créer une page index.md. Il suffit alors d'ouvrir mdwiki.html dans un navigateur pour admirer votre réalisation.
MDWiki comporte plusieurs fonctionnalités qui permettent une utilisation très accessible via la syntaxe markdown ainsi que des utilisations plus poussées avec les gimmicks.

