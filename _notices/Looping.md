---
layout: notice

nom: "Looping"
date_creation: "Vendredi, 11 mai, 2018 - 02:05"
date_modification: "Dimanche, 2 février, 2020 - 02:37"
logo: 
    src: "https://framalibre.org/sites/default/files/leslogos/Looping.jpg"
site_web: "https://www.looping-mcd.fr"
plateformes: "Windows"
langues: "Français"
description_courte: "Modélisation Conceptuelle de Données
(nouvelle version 2.5)"
createurices: "BERGOUGNOUX Patrick, MATHET Eric"
licences: "Domaine public"
mots_clefs: "MCD, MLD, SQL, modélisation, base de données, Merise, développement"
lien_wikipedia: ""
lien_exodus: ""
---

INSTALLATION
- Compatible Windows 7 et supérieure, 32 et 64 bits
- Fonctionne avec Wine sous macOS et Linux
- Installation par simple copie du fichier "Looping.exe"
- Exécutable depuis tout support : disque local, serveur, clé USB
FONCTIONNALITES (v2.5)
- Modélisation Entités/Associations et Diagramme de Classes UML
- Classes d'entités fictives non générées dans le MLD
- Transformation association en entité avec identifiants relatifs
- Héritage avec spécialisations et généralisations
- Contraintes d'intégrité fonctionnelle (CIF) et inter-associations
- Règles de gestion avec possibilité de spécifier du code SQL
- Insertion de textes, graphiques et images connectables par flux
- Modèle Logique de Données (MLD) textuel affiché en temps-réel
- Requêtes SQL de création des tables de la BD en temps-réel
- Export scripts SQL pour les principaux SGBD du marché
- Exportation MCD aux formats images et presse-papiers
- Présentation personnalisable multi-vue et multi-zoom

