---
nom: "Simple Backup"
date_creation: "Dimanche, 14 janvier, 2018 - 18:07"
date_modification: "Samedi, 15 juin, 2019 - 21:49"
logo:
    src: "images/logo/Simple Backup.jpg"
site_web: "https://framagit.org/zenjo/simplebackup"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
langues:
    - "Français"
    - "English"
description_courte: "Script bash de backup incrémentaux, pour répertoires locaux ou réseau et sites distants via ssh."
createurices: "Robert Sebille"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "ssh"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/simple-backup"
---

Script bash de backup incrémentaux, pour répertoires locaux ou réseau et sites distants via ssh.Fonctionnalités
Synchronisation incrémentale de répertoires locaux, réseaux ou distants (via ssh).
Supporte une clé privée ssh pour la connexion aux serveurs distants.
Nomage des répertoires de backups au format backupAAAAMMJJ_HHMMSS.0, .1, ... .n
Options en ligne de commande pour afficher l'aide, la version, les codes de retour et changer la langue d'affichage.

Fichiers de configurations des backups propres à SimpleBackup.
Fichiers d'exclusion de répertoires et/ou fichiers des backups - uniquement ceux des scripts SimpleBackup.
Explorateur de backups et gestion des restores - uniquement ceux des scripts SimpleBackup.
 Actuellement 2 langues, FR et EN.

Vérifications diverses
Documentation complète

