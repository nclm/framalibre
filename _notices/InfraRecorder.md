---
nom: "InfraRecorder"
date_creation: "Dimanche, 2 avril, 2017 - 10:17"
date_modification: "Mardi, 11 mai, 2021 - 23:13"
logo:
    src: "images/logo/InfraRecorder.png"
site_web: "http://infrarecorder.org/"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La gravure en toute liberté sous Windows"
createurices: "Christian Kindahl"
alternative_a: "Nero, Roxio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "gravure"
    - "cd-rom"
lien_wikipedia: "https://fr.wikipedia.org/wiki/InfraRecorder"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/infrarecorder"
---

InfraRecorder est un logiciel de gravure de CD et de DVD pour Windows.
Il permet aisément de créer ses propres compilations de musique libre, de créer des sauvegardes de ses données, de copier des distributions Linux...
Il reconnait tous les formats les plus populaires de fichiers.
Il supporte tout type de support CD et DVD.
Même si le logiciel n'a plus été mis à jour depuis un bon moment, la dernière version proposée est pleinement fonctionnelle.

