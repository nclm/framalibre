---
nom: "Text2quiz"
date_creation: "Mardi, 13 juin, 2023 - 10:58"
date_modification: "Mardi, 13 juin, 2023 - 11:03"
logo:
    src: "images/logo/Text2quiz.png"
site_web: "https://text2quiz.vercel.app/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
description_courte: "Outil en ligne, libre et gratuit, qui permet de créer automatiquement toutes sortes de quiz"
createurices: "Cédric Eyssette"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "education"
    - "qcm"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/text2quiz"
---

Avantages :
* Pas de compte à créer
* Pas de cookies, pas de données enregistrées : totalement compatible avec le RGPD
* Interface simple pour pouvoir créer rapidement un quiz
Comment ça marche ?
Pour écrire son quiz, on doit coder chaque question en respectant une syntaxe simple
On peut afficher l'aide dans la fenêtre d'édition du quiz, et commencer d'abord par les types de question les plus simples
Types de questions possibles

Vrai ou faux ?
QCM
Question / réponse
Question à réponse courte
Texte à trous
Texte à trous avec réponses courtes
Flashcard
Grille
Ordre
Étiquettes
Association
Correspondance
Correspondance stricte


