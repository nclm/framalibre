---
nom: "Miro Video Converter"
date_creation: "Mercredi, 5 avril, 2017 - 15:24"
date_modification: "Lundi, 10 mai, 2021 - 13:29"
logo:
    src: "images/logo/Miro Video Converter.png"
site_web: "http://www.mirovideoconverter.com"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "MiroVideoConverter permet très simplement de convertir (et compresser) vos sons et vidéos."
createurices: ""
alternative_a: "Formatfactory, FreemakeVideoConverter, MediaCoder"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "convertisseur"
    - "vidéo"
    - "audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/miro-video-converter"
---

En quelques clics, MiroVideoConverter permet de convertir (et compresser) vos sons et vidéos dans les formats suivants :
- audio : MP3 / OGG Vorbis
- vidéo : WebM / MP4 (H264, compatible HTML5) / Ogg Theora
Ce logiciel, très simple, ne semble malheureusement plus maintenu aujourd'hui (dernière version publiée en 2015).

