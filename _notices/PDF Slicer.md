---
nom: "PDF Slicer"
date_creation: "Jeudi, 4 août, 2022 - 23:17"
date_modification: "Vendredi, 5 août, 2022 - 00:11"
logo:
    src: "images/logo/PDF Slicer.png"
site_web: "https://junrrein.github.io/pdfslicer/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un logiciel simple pour extraire, assembler, pivoter et réordonner des pages de documents PDF."
createurices: "Julián Unrrein"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "pdf"
    - "manipulation de pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/pdf-slicer"
---

Un logiciel simple pour extraire, assembler, pivoter et réordonner des pages de documents PDF.
Les transformations peuvent être réalisées sur une ou plusieurs pages. Il est aussi possible d'annuler ou reprendre les modifications.

