---
nom: "Scribe"
date_creation: "lundi, 25 décembre, 2023 - 22:14"
date_modification: "lundi, 25 décembre, 2023 - 22:14"
logo:
    src: "images/logo/Scribe.png"
site_web: "https://scribe.cemea.org/"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Scribe transcrit votre fichier audio en texte écrit"
createurices: "Ceméa"
alternative_a: ""
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "transcription"
    - "sous-titres"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"

---

Pour faciliter le travail fastidieux de retranscription, Scribe automatise une partie du travail en réalisant une retranscription automatique depuis :
- votre fichier audio
- le lien d'une vidéo Peertube, Youtube, Vimeo, Dailymotion...

Une fois la retranscription réalisée, elle vous est envoyée par mail.
