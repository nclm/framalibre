---
nom: "JDownloader"
date_creation: "Vendredi, 18 décembre, 2020 - 22:20"
date_modification: "Vendredi, 18 décembre, 2020 - 22:20"
logo:
    src: "images/logo/JDownloader.png"
site_web: "https://jdownloader.org/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "JDownloader est un gestionnaire de téléchargements écrit en java."
createurices: ""
alternative_a: "GetRight, Download Accelerator"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/jdownloader"
---

Il vous permet de télécharger des fichiers de divers hébergeurs avec ou sans compte premium. Environ 110 hébergeurs et plus de 300 plugins de décryptage + extraction automatique des fichiers + Copie automatique du presse papier. C'est la Rolls du téléchargement !
Il permet l'ajout des liens automatiquement - il suffit de les copier, par exemple dans Firefox ou ailleurs - et ils se rajouteront dans JDownloader.
Télécharger plusieurs fichiers en même temps
Télécharger avec des connexions multiples
JD possède un puissant OCR (JAntiCaptcha)
Décrypte les conteneurs RSDF, CCF and DLC
Décrypte les plugins pour de nombreux services. Exemple : sj.org, UCMS, WordPress, RLSLog….
Téléchargement depuis Youtube, Vimeo, Clipfish video et Mp3
Extracteur automatique (inclus liste de recherche de mot de passe) (archives RAR)
Support thématique

