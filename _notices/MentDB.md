---
nom: "MentDB"
date_creation: "Samedi, 27 juillet, 2019 - 02:42"
date_modification: "Lundi, 27 avril, 2020 - 10:54"
logo:
    src: "images/logo/MentDB.png"
site_web: "https://www.mentdb.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Strong AI and World Wide Data"
createurices: "Jimmitry Payet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "programmation"
    - "base de données"
    - "intelligence artificielle"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mentdb"
---

Bienvenue sur MentDB (base de données Mentalais). La plate-forme fournit des outils pour l'IA, la SOA, l'ETL, l'ESB, la base de données, l'application Web, la qualité des données, l'analyse prédictive, le chatbot, etc., dans un langage de données révolutionnaire (MQL). Le serveur est basé sur une nouvelle génération d'algorithme d'intelligence artificielle et sur une couche SOA innovante pour atteindre le WWD.

