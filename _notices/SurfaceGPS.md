---
nom: "SurfaceGPS"
date_creation: "Lundi, 4 décembre, 2017 - 21:00"
date_modification: "Vendredi, 7 mai, 2021 - 11:02"
logo:
    src: "images/logo/SurfaceGPS.png"
site_web: "https://play.google.com/store/apps/details?id=fr.joliciel.surfacegps"
plateformes:
    - "Android"
langues:
    - "Français"
    - "English"
description_courte: "Surface GPS est une application qui calcule la surface d'une parcelle de terrain à l'aide du GPS du smartphone"
createurices: "Laurent Bouquet"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "métiers"
    - "calcul"
    - "gps"
    - "agriculture"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/surfacegps"
---

Surface GPS est une application libre qui calcule la surface d'une parcelle de terrain en m², avec le capteur GPS de votre smartphone. C'est pratique pour les agriculteurs, géomètres, ou même les jardiniers.
Comment utiliser SurfaceGPS ?
Etape 1 :
Dans la liste des parcelles, cliquez en haut à droite pour créer une parcelle, donnez lui un nom, puis cliquez en haut à droite pour l'enregistrer.
Etape 2 :
Cliquez sur la parcelle dans la liste des parcelles, cliquez sur 'Enregistrer' et déplacez vous sur le terrain.
L'application va mémoriser vos positions, au fur et à mesure de votre déplacement.
Une fois arrivé au point de départ, cliquez sur 'Arrêter', et la surface en m² s'affiche en haut à droite.

