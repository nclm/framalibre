---
nom: "Boostnote"
date_creation: "Mercredi, 10 avril, 2019 - 19:51"
date_modification: "Mercredi, 10 avril, 2019 - 19:51"
logo:
    src: "images/logo/Boostnote.png"
site_web: "https://boostnote.io/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Android"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un logiciel de prise de notes simples et complets pour les développeurs."
createurices: "BoostIO"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "prise de notes"
    - "notes"
    - "markdown"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/4183/"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/boostnote"
---

Boostnote est un logiciel de prise de note en format markdown avec visualisation du rendu automatique. Il s'agit à la base d'un logiciel développé pour les développeurs informatiques, mais il est très simple à prendre en main et agréable d'utilisation. On peut le coupler avec le logiciel Nextcloud pour avoir un accès à ces notes depuis plusieurs ordinateurs. Une fonction « snippets » (de l'anglais voulant dire bribes ou extraits) permet de sauvegarder des bouts de codes en de nombreux langages informatiques (plus de 100) et de les rangers avec les notes.

