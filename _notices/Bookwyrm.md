---
nom: "Bookwyrm"
date_creation: "mardi, 13 février, 2024 - 15:58"
date_modification: "mardi, 13 février, 2024 - 15:58"
logo:
    src: "images/logo/Bookwyrm.ico"
site_web: "https://joinbookwyrm.com"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "BookWyrm est un réseau social pour garder la trace de vos lectures, parler de livres, écrire des critiques et découvrir quoi lire ensuite."
createurices: "https://www.mousereeve.com/"
alternative_a: "Goodreads , Babelio"
licences:
    - "Autre"
tags:
    - "fediverse"
    - "lecture"
    - "réseau social"
    - "livres"
    - "bibliothèque"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/nl.privacydragon.bookwyrm/latest/"
identifiant_wikidata: "Q110943250"
mis_en_avant: "non"

---

BookWyrm est un réseau social pour garder la trace de vos lectures, parler de livres, écrire des critiques et découvrir quoi lire ensuite.

## Gardez la trace de vos lectures

Et de celles d’autres lecteurs et lectrices
Mettez à jour votre statut de lecture quand vous commencez et terminez des livres, publiez des mises à jour au fur et à mesure, et définissez un but annuel de livres lus.

## Découvrez des livres

Les recommandations viennent des gens, et non des algorithmes
Restez au courant de ce que vos ami·es lisent, et lisez les opinions de votre communauté.

## Partagez votre avis

Comme vous le voulez
Écrivez des critiques ou publiez des commentaires en passant, et contrôlez qui a accès à vos publications grâce à des réglages avancé.
