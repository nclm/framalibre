---
nom: "Mat"
date_creation: "Dimanche, 15 janvier, 2017 - 21:11"
date_modification: "Mercredi, 12 mai, 2021 - 16:07"
logo:
    src: "images/logo/Mat.png"
site_web: "https://mat.boum.org/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Mat permet de supprimer les metadonnées de vos documents afin de respecter votre vie."
createurices: "Julien Voisin"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "vie privée"
    - "anonymat"
    - "métadonnées"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mat"
---

Mat (Metadata Anonymisation Toolkit) permet de supprimer les metadonnées de vos documents afin de les anonymiser.
Il prend en charge différents formats de documents (jpg,png,ogg,pdf,flac,ogg,etc...).
Son utilisation est très simple (glisser-déposer + nettoyer).

