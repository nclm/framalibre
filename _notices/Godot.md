---
nom: "Godot"
date_creation: "Vendredi, 6 janvier, 2017 - 15:26"
date_modification: "Mercredi, 12 mai, 2021 - 16:55"
logo:
    src: "images/logo/Godot.png"
site_web: "https://godotengine.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Godot est un moteur et un environnement de développement de jeu vidéo 2D/3D."
createurices: "Juan Linietsky, Ariel Manzur"
alternative_a: "Unity2D, Unity3D, Unreal Engine, GameMaker Studio, Construct"
licences:
    - "Licence MIT/X11"
tags:
    - "création"
    - "jeu vidéo"
    - "environnement de développement"
    - "développement"
    - "développement de jeu vidéo"
    - "moteur de jeu"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Godot_(moteur_de_jeu)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/godot"
---

Godot est un moteur et un environnement de développement de jeu vidéo 2D/3D. Il dispose d'un éditeur complet que l'on peut comparer à celui de Unity3D, comprenant notamment :
- Un IDE complet pour l'écriture des scripts
- D'outils d'analyse de performances et de débogage
- Des outils d'animations
- Un éditeur de scène
Le scripting se fait en GDScript, un langage à la syntaxe proche de Python, mais Godot dispose aussi d'une
API permettant de coder un C++ et bientôt en C#. La structure des projets est parfaitement adaptée au versionnement avec des outils comme Git.
Le moteur supporte l'export de projets à destination de multiples plateformes :
- PC (GNU/Linux, BSD, Windows, OSX, …)
- Mobile (iOS, Android)
- Web (HTML5)

