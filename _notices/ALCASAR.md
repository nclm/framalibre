---
nom: "ALCASAR"
date_creation: "Samedi, 1 septembre, 2018 - 18:04"
date_modification: "Samedi, 1 septembre, 2018 - 18:16"
logo:
    src: "images/logo/ALCASAR.jpeg"
site_web: "http://www.alcasar.net"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "ALCASAR est un controleur d'accès au réseau (N.A.C. Network Access Controler)"
createurices: "Richard REY, Franck BOUIJOUX, Stephane Weber, Pascal LEVANT"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "sécurité"
    - "authentification"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/alcasar"
---

ALCASAR est un contrôleur sécurisé d'accès à Internet pour les réseaux publics ou domestiques. Il authentifie et protège les connexions des utilisateurs indépendamment de leurs équipements (PC, tablette, smartphone, console de jeux, TV, etc.). Il intègre plusieurs mécanismes de filtrage adaptés aux besoins privés (contrôle parental et filtrage des objets connectés) ou aux chartes et règlements internes (entreprises, associations, centres de loisirs et d'hébergement, écoles, bibliothèques, mairies, etc.).

