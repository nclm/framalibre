---
nom: "mdbook"
date_creation: "Jeudi, 23 février, 2023 - 20:35"
date_modification: "Jeudi, 23 février, 2023 - 20:35"
logo:
    src: "images/logo/mdbook.png"
site_web: "https://github.com/rust-lang/mdBook"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Un générateur de sites statiques à partir de fichiers Markdown"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Mozilla (MPL)"
tags:
    - "cms"
    - "markdown"
    - "web"
    - "documentation"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mdbook"
---

Mdbook est un générateur de site statique à partir de fichiers en Markdown.
Il est plutôt orienté pour réaliser des sites de documentation, à la manière de sphinx.
Le thème par défaut est très simple mais propre et agréable.
Il intègre une indexation statique automatique, reste très simple d'utilisation et la documentation est vraiment agréable.
Petit bonus: il est possible d'ajouter des plugins, dont un plugin mermaid.js, qui permet donc d'intégrer des graphes au format Markdown.

