---
nom: "AsmBB"
date_creation: "Mardi, 12 février, 2019 - 14:30"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/AsmBB.png"
site_web: "https://board.asm32.info/"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Un moteur de forum entièrement écrit en Assembleur !"
createurices: "John Found"
alternative_a: "vBulletin, Invision Community, XenForo, Vanilla"
licences:
    - "Licence Publique Union Européenne (EUPL)"
tags:
    - "internet"
    - "forum de discussion"
    - "communauté"
    - "communication"
    - "discussion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/asmbb"
---

AsmBB est un moteur de forum simple qui a l'originalité d'être écrit en langage Assembleur. ASMBB est adapté à la lecture sur mobile et utilise SQLite comme base de données. Il reste pourtant simple à installer et à configurer. Un tout petit VPS ou un hébergement mutualisé permet de le faire tourner sans problème.
La création de thèmes est très simple avec quelques connaissances en CSS. L'interface est traduite en différentes langues dont l'anglais, le français, l'allemand, le russe, le bulgare.

