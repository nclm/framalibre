---
nom: "Seyes"
date_creation: "dimanche, 31 mars, 2024 - 08:55"
date_modification: "dimanche, 31 mars, 2024 - 08:55"
logo:
    src: "images/logo/Seyes.png"
site_web: "https://educajou.forge.apps.education.fr/seyes/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Seyes affiche une page quadrillée de type cahier, et permet d'y ajouter du texte."
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "écriture"
    - "cursive"
    - "école"
    - "tableau"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Seyes affiche une page quadrillée de type cahier, et permet d'y ajouter du texte. 
Elle fonctionne à la manière d'un traitement de texte : pour insérer de nouvelles lignes, utilisez la touche "entrée".
On peut écrire dans la marge et dans la zone principale.
## Fonctionnalités
- choix de la police
- coloration du texte
- soulignement du texte en rouge
- choix de la taille de police et des carreaux (qui s'adaptent automatiquement)
- réglage de la largeur de marge
## Openboard
Une [extension pour Openboard](https://forge.apps.education.fr/educajou/seyes/-/blob/main/Seyes.wgt.zip?ref_type=heads) est également disponible.
