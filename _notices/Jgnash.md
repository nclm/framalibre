---
nom: "Jgnash"
date_creation: "Vendredi, 4 novembre, 2022 - 20:58"
date_modification: "Jeudi, 16 février, 2023 - 21:51"
logo:
    src: "images/logo/Jgnash.png"
site_web: "https://jgnash.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Logiciel de comptabilité personnelle simple, efficace, sérieux et facile à comprendre."
createurices: ""
alternative_a: "Grisbi, MoneyManager Ex, GNUcash"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "java"
lien_wikipedia: "https://fr.wikipedia.org/wiki/JGnash"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/jgnash"
---

Ce logiciel libre existe depuis de nombreuses années : il est régulièrement maintenu. Son interface graphique est très simple tout en étant facile à utiliser, il gère la comptabilité à partie double ce qui est un gage de sérieux pour ceux qui ont des bases en comptabilité (Si vous ne savez pas de quoi il s'agit, vous n'aurez pas de mal à l'utiliser non plus).
Il convient autant à des débutants en gestion de comptes que pour des personnes souhaitant un logiciel simple pour gérer des comptes modérément complexes (gestion de plusieurs devises possible, récupération des comptes de votre banque, création de graphismes pour se rendre compte de l'évolution des dépenses et des recettes..., gestion de budgets prévisionnels... ).
ATTENTION : Le site officiel n'est pas à jour pour l'instant : le lien de téléchargement vous propose une ancienne version.
Il peut aussi être utilisé pour une association...

