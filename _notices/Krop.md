---
nom: "Krop"
date_creation: "mardi, 12 mars, 2024 - 10:58"
date_modification: "mercredi, 13 mars, 2024 - 16:11"
logo:
    src: "images/logo/Krop.png"
site_web: "https://arminstraub.com/software/krop"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Ouitl de découpage de PDF. Permet, notamment, de ne sélectionner qu'une partie d'une page PDF (enlever les publicités par exemple), redimensionner un fichier PDF pour l'imprimer sans perte de papier…"
createurices: "Armin Straub"
alternative_a: "Adobe Acrobat"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "manipulation de pdf"
    - "pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Sélectionne la ou les zones à découper, laisse Krop reproduire la découpe sur toutes les pages de ton PDF et tu pourra générer un nouveau fichier PDF mis en forme selon tes besoins. Très pratique pour supprimer des publicités d'un article avant de le diffuser sur papier ou web. Permet de séparer des colonnes d'articles pour en faire des pages séparées, de redécouper un article présentant deux pages sur une seule…
