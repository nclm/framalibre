---
nom: "Manuskript"
date_creation: "Mardi, 12 mai, 2020 - 17:26"
date_modification: "Mercredi, 12 mai, 2021 - 15:52"
logo:
    src: "images/logo/Manuskript.png"
site_web: "http://www.theologeek.ch/manuskript/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Ne vous racontez pas d'histoire, stimulez votre inspiration et composer votre récit et publiez-le!"
createurices: "Olivier Kes"
alternative_a: "Scrivener"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "écriture"
    - "ebook"
    - "éditeur"
    - "roman"
    - "nouvelle"
    - "pdf"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/manuskript"
---

Manuskript est un logiciel d’assistance à l’écriture. Disons une sorte d’écritoire numérique, un cabinet d’écriture qui va vous aider à structurer vos idées, construire vos personnages, mondes et intrigues.
Aux espaces de préparation, s’ajoute une interface d’écriture non-distractive (adieux boutons, listes déroulantes d’un logiciel de bureautique). Pour analyser votre création, vous avez des statistiques, des repères visuels pour doser les temps forts de vos intrigues.
Au final vous produirez un document texte dans différents notamment des formats epub pour faciliter votre diffusion sur des liseuses.

