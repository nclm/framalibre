---
nom: "FluxBB"
date_creation: "Mardi, 12 février, 2019 - 14:04"
date_modification: "Lundi, 10 mai, 2021 - 14:40"
logo:
    src: "images/logo/FluxBB.png"
site_web: "https://fluxbb.org/"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Un forum stable, fonctionnel et rapide"
createurices: "Franz Liedke"
alternative_a: "vBulletin, Invision Community, XenForo, Vanilla"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "forum de discussion"
    - "discussion"
    - "communauté"
    - "communication"
    - "php"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FluxBB"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fluxbb"
---

Un moteur de forum simple à utiliser et à installer. FluxBB se concentre sur l'essentiel avec des options d'administration, de permissions et de modérations complètes. Il est écrit en PHP.
Il est similaire à phpBB sans apport superflu de fonctionnalités et est généralement plus rapide.

