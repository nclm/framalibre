---
nom: "WireShark"
date_creation: "Samedi, 22 juillet, 2017 - 08:51"
date_modification: "Mercredi, 12 mai, 2021 - 15:14"
logo:
    src: "images/logo/WireShark.png"
site_web: "https://www.wireshark.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Un analyseur réseau multi-protocoles pour visualiser les échanges sur les réseaux de communication."
createurices: "Gerald Combs"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
    - "réseau"
    - "protocoles"
    - "sécurité"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Wireshark"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/wireshark"
---

WireShark est un analyseur de trames réseaux. Le logiciel écoute les réseaux sur une ou plusieurs interfaces et capture les échanges en mode global ou sélectif.
Les trames capturées peuvent être analysées à la fin de la capture. Il est également possible de les enregistrer dans un fichier compressé pour une analyse ultérieure.
L'analyse des trames permet d'établir un certain nombre d'informations statistiques qui enrichissent la connaissance des données qui transitent sur les réseaux.
Les technologies suivantes sont supportées : Ethernet, IEEE 802.11, PPP/HDLC, ATM, Bluetooth, USB, Token Ring, Frame Relay, FDDI, ...
WireShark supporte le décryptage IPsec, ISAKMP, Kerberos, SNMPv3, SSL/TLS, WEP, and WPA/WPA2.
L'analyse du protocole TCP/IP prend en compte les versions IPv4 et IPv6.
Un superbe outil pour ceux qui souhaitent explorer les échanges réseau.

