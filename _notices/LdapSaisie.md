---
nom: "LdapSaisie"
date_creation: "mardi, 16 avril, 2024 - 00:14"
date_modification: "mardi, 16 avril, 2024 - 00:14"


site_web: "https://ldapsaisie.org/fr/"
plateformes:
    - "le web"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Application web d'administration d'annuaire LDAP"
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "LDAP"
    - "annuaire"
lien_wikipedia: "https://fr.wikipedia.org/wiki/LdapSaisie"
lien_exodus: ""
identifiant_wikidata: "Q3220124"
mis_en_avant: "non"

---

LdapSaisie est une application web d'administration d'annuaire LDAP. Elle a pour but d'abstraire la complexité d'un annuaire par l'intermédiaire d'une interface d'administration simple et intuitive. L'application a été conçue avec pour objectif premier une modularité maximum, ce qui permet l'extension ou l'adaptation facile de l'application par l'intermédiaire de modules, d'extensions et de greffons. Cette application peut être utilisée pour administrer le système d'informations basé sur l'annuaire LDAP et également, en parallèle, permettre aux utilisateurs d'avoir accès aux données les concernant, et éventuellement de les modifier.

Fonctionnalités principales :
et complète est fournie
* Gestion d'un ou plusieurs annuaires pouvant présenter des ressemblances ou être complètement différents;
* Gestion d'annuaires simples et multi-branches;
* Gestion d'un nombre illimité de types d'objets dont le schéma est complètement libre et personnalisable;
* Gestion d'un nombre illimité de populations se connectant à l'interface;
* Gestion fine des droits des utilisateurs, permettant la maitrise des droits d'accès sur les objets de l'annuaire et leurs attributs tout en permettant la délégation de droits;
* Gestion d'un grand nombre de types d'attributs. Chaque type d'attribut a des fonctionnalités qui lui sont propres et qui rendent plus facile et agréable l'utilisation de l'interface (génération automatique de mot de passe, génération des valeurs d'un champ à partir d'autres, ...);
* Gestion d'un grand nombre de règles de vérification des valeurs des attributs;
* Gestion simplifiée des relations entre les objets de l'annuaire;
* Interface facilement personnalisable grâce à l'utilisation d'un système de modèles. Une feuille CSS paramétrable est également fourni pour une personnalisation minimaliste : définissez juste vos couleurs, votre logo et le tour est joué;
* Possibilité de positionner des déclencheurs permettant d'exécuter vos propres scripts, fonctions ou méthodes aux moments précis où l'utilisateur crée, modifie ou supprime un objet ou un de ses attributs. Ces déclencheurs, en fonction de leur positionnement, peuvent influencer le comportement de l'application en empêchant par exemple, la validation des données d'un formulaire;
* Gestion fine de l'affichage des attributs en fonction de l'écran (=vue) sur lequel se trouve l'utilisateur;
* Gestion des dépendances entre attributs, permettant par exemple, de regénérer automatiquement la valeur d'un attribut (caché ou non) lors de la modification d'un autre;
* Possibilité de gérer des attributs entièrement cachés, dont les valeurs seront modifiées lors de la modification d'attributs en dépendance;
* Une API REST est également fournie pour permettre d'automatiser toutes vos manipulations habituelles dans l'annuaire en écrivant vos propres scripts, applications tierces ou encore playbook Ansible;
* Un puissant mécanisme permet de surcharger tout ou partie de l'application, que ce soit du code, un script Javascript, une feuille de style CSS ou encore une image. Cela permet de maintenir facilement et proprement votre installation dans le temps tout en profitant des mises à jours.

Pour finir, une documentation riche et complète est fournis en français. Celle-ci étant mise à jour en parallèle de toutes modifications du code, elle se veut exhaustive.
