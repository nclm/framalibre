---
nom: "Nootka"
date_creation: "Mercredi, 1 novembre, 2023 - 17:48"
date_modification: "Mercredi, 1 novembre, 2023 - 18:05"
logo:
    src: "images/logo/Nootka.png"
site_web: "https://nootka.sourceforge.io"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Éduquer son oreille, ses doigts et sa lecture des portées."
createurices: "Seelook"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "musique"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/nootka"
---

Nootka (petite note en Polonais) est un logiciel d'apprentissage de l'emplacement des notes et leur reconnaissance sur son instrument. L'application propose de nombreux exercices (et un éditeur d'exercice) avec des niveaux de difficultés croissant. On doit trianguler dans tous les sens entre la représentation du toucher de l'instrument, le nom des notes et leur placement sur la portée.
Le logiciel est conçu pour les guitares (électrique et classique), les pianos, yukulélés, bandonéons, basses, saxophones et la possibilité d'en décrire de nouveaux.
Le site propose une base d'exercices que l'on peut enrichir.
Si votre instrument est branché à l'ordinateur et pareil pour un micro, l'application utilise la reconnaissance sonore pour détecter les notes que vous exécutez.

