---
nom: "Tux Paint"
date_creation: "Mardi, 14 mars, 2017 - 22:22"
date_modification: "Lundi, 10 mai, 2021 - 14:22"
logo:
    src: "images/logo/Tux Paint.png"
site_web: "http://www.tuxpaint.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel de dessin pour les enfants... et les plus grands."
createurices: "Bill Kendrick"
alternative_a: "Microsoft Paint, Paint.net"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "graphisme"
    - "éducation"
    - "dessin"
    - "enfant"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tux_Paint"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tux-paint"
---

Tux Paint est un logiciel de dessin libre, conçu pour les enfants (la cible est ceux de 3 ans à 12 ans). Il permet de réaliser facilement des dessins grâce à une interface claire, facile à utiliser. La prise en main est ainsi extrêmement rapide même par des personnes n'ayant jamais utilisé de logiciel de dessin.
De nombreux tampons et des effets sonores ajoutent aussi à l'attractivité du logiciel.
Tux Paint est prisé dans le monde de l’éducation et est utilisé dans de nombreuses écoles maternelles et élémentaires.

