---
nom: "Imagination"
date_creation: "Dimanche, 9 juillet, 2023 - 22:06"
date_modification: "Dimanche, 9 juillet, 2023 - 22:06"
logo:
    src: "images/logo/Imagination.png"
site_web: "https://imagination.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "BSD"
langues:
    - "English"
description_courte: "Un créateur de diaporama léger et facile à utiliser pour Linux et FreeBSD écrit en C et construit avec GTK3"
createurices: ""
alternative_a: "Movavi Slideshow Maker, InVideo, Ashampoo Slideshow Studio"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "diaporama"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/imagination"
---

Imagination est un créateur de diaporamas DVD léger et simple pour GNU/Linux. Imagination a été conçu dès le départ pour être rapide, léger et facile à utiliser. Il nécessite l'encodeur ffmpeg pour produire le fichier vidéo et libsox pour gérer l'audio.
Imagination propose actuellement 69 effets de transition, une fonction aléatoire pour définir automatiquement une transition aléatoire sur toutes les diapositives sélectionnées, une capacité de couper/copier/coller sur les diapositives, la capacité de Ken Burns, du texte sur les diapositives avec quelques animations de texte, la possibilité d'ajouter un vide diapositive avec un éditeur de dégradé et exportation du diaporama au format OGV Theora/Vorbis, vidéo FLV grand écran et 3GP pour les téléphones mobiles.

