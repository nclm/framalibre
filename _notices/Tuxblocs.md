---
nom: "Tuxblocs"
date_creation: "Jeudi, 4 mai, 2023 - 20:37"
date_modification: "dimanche, 31 mars, 2024 - 08:34"
logo:
    src: "images/logo/Tuxblocs.png"
site_web: "https://educajou.forge.apps.education.fr/tuxblocs/"
plateformes:
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Manipulation de blocs de numération"
createurices: "Arnaud Champollion"
alternative_a: "NLVM Blocs de base"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "mathématiques"
    - "numération"
    - "primaire"
    - "dizaines"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tuxblocs"
---

Tuxblocs est un logiciel qui permet de représenter les milliers, centaines, dizaines et unités sous formes de blocs de base, et d’effectuer des conversions entre colonnes.
Il ne s'agit pas d'un exerciseur autonome pour l'élève, bien qu'un mode "interroger" existe.
C'est un outil avec de nombreuses fonctions qui permettent à l'enseignant d'animer une séance de numération.
