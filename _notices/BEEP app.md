---
nom: "BEEP app"
date_creation: "jeudi, 4 janvier, 2024 - 12:23"
date_modification: "jeudi, 4 janvier, 2024 - 12:23"
logo:
    src: "images/logo/BEEP app.png"
site_web: "https://beep.nl/beep-app"
plateformes:
    - "le web"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "BEEP est une application de gestion et de surveillance de rucher pour les apiculteurs."
createurices: ""
alternative_a: "Beekube"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "gestion"
    - "apiculture"
    - "application web"
    - "base de données"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

BEEP est une application de gestion et de surveillance de rucher pour les apiculteurs.

L'application facilite les apiculteurs à prendre soin des colonies d'abeilles. Elle permet la gestion de rucher dans laquelle vous pouvez suivre toutes vos actions et observations. 

Elle est basée sur une base de données contenant des termes importants pour le soin des colonies d'abeilles. Vous pouvez concevoir vous-même votre fiche de suivi des colonies. Vous avez toujours les informations saisies à portée de main et sur n'importe quel appareil connecté à internet. Elle permet également de partager ces donnes pour la recherche.

Il est aussi possible de connecter des capteurs pour enregistrer des données physiques. Par exemple avec "la base BEEP" en open hardware.
