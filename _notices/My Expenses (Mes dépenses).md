---
nom: "My Expenses (Mes dépenses)"
date_creation: "Vendredi, 20 août, 2021 - 00:24"
date_modification: "Vendredi, 20 août, 2021 - 00:31"
logo:
    src: "images/logo/My Expenses (Mes dépenses).png"
site_web: "https://www.myexpenses.mobi/"
plateformes:
    - "Android"
langues:
    - "Autres langues"
description_courte: "Une solution de gestion de finances personnelles facile et efficace sous Android"
createurices: "Michael Totschnig"
alternative_a: "My Budget Book, 1Money"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "finances"
    - "budget"
lien_wikipedia: ""
lien_exodus: "https://reports.exodus-privacy.eu.org/fr/reports/org.totschnig.myexpenses/latest…"
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/my-expenses-mes-d%C3%A9penses"
---

My Expenses (Mes dépenses) est une application de gestion de finances personnelles autorisant la gestion de différents types de comptes : comptes bancaires, comptes d'actif ou de passif, carte bancaire… Il propose les fonctionnalités attendues de ce type de logiciels : pointage et rapprochement des écritures, gestion des modèles et échéances, etc. La saisie semi-automatique des opérations (ex : la catégorie de l'opération est déduite du tiers) rend celle-ci aisée et rapide.
Un certain nombre de fonctionnalités (telle la synchronisation via Webdav, le "mode scan" permettant de scanner un ticket de caisse et d'en extraire le total) ne sont accessibles qu'après l'achat d'une licence.

