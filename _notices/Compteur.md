---
nom: "Compteur"
date_creation: "lundi, 29 avril, 2024 - 15:14"
date_modification: "lundi, 29 avril, 2024 - 15:14"
logo:
    src: "images/logo/Compteur.png"
site_web: "https://educajou.forge.apps.education.fr/compteur/"
plateformes:
    - "le web"
    - "Apple iOS"
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
    - "Autre"
langues:
    - "Français"
description_courte: "Un compteur interactif dans le style \"compteur kilométrique\""
createurices: "Arnaud Champollion"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "éducation"
    - "mathématiques"
    - "cycle 2"
    - "cycle 3"
    - "numération"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"

---

Cette application affiche un compteur de type "compteur kilométrique", avec des intitulés de colonnes (unités, dizaines, centaines, milliers, millions ... ainsi que les décimales : dixièmes, centièmes, millièmes) et des boutons pour les incrémenter.
On peut s'en servir pour réfléchir, anticiper et observer les faits numériques qui se produisent lors de l'addition ou de la soustraction d'une dizaine entière, d'une centaine, d'un millier ... 
... voire de plusieurs d'entre eux. (ex : "ajouter 5 centaines") avec un rendu animé.
