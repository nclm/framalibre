---
nom: "JabRef"
date_creation: "Mardi, 24 octobre, 2017 - 15:14"
date_modification: "mardi, 26 décembre, 2023 - 13:36"
logo:
    src: "images/logo/JabRef.png"
site_web: "https://www.jabref.org"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "JabRef permet de gérer vos références bibliographiques."
createurices: "JabRef Team"
alternative_a: "EndNote"
licences:
    - "Licence MIT/X11 (MIT)"
tags:
    - "bureautique"
    - "bibliographie"
    - "référence bibliographique"
    - "latex"
lien_wikipedia: "https://fr.wikipedia.org/wiki/JabRef"
lien_exodus: ""
identifiant_wikidata: "Q1676802"
mis_en_avant: "oui"
redirect_from: "/content/jabref"
---

JabRef est un logiciel de gestion de références bibliographiques libre, gratuit et à source ouverte.
JabRef utilise BibTeX et BibLaTeX comme formats natifs, ce qui le rend particulièrement adapté pour l’utilisation de LaTeX. Il est possible de travailler à plusieurs, soit en partageant un fichier .bib, soit en utilisant une base de données SQL.
Jabref est capable d’ajouter des références dans divers éditeurs de texte, dont Microsoft Word et LibreOffice Writer.
JabRef peut importer des références à partir de bases de données scientifiques en ligne.


