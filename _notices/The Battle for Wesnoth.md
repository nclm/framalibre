---
nom: "The Battle for Wesnoth"
date_creation: "Lundi, 2 mars, 2015 - 20:34"
date_modification: "Dimanche, 2 avril, 2017 - 00:49"
logo:
    src: "images/logo/The Battle for Wesnoth.png"
site_web: "http://wesnoth.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "La Bataille pour Wesnoth est un jeu vidéo de stratégie au tour par tour dans un univers médiéval fantastique."
createurices: "David White"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "stratégie"
    - "tour par tour"
    - "multi-joueur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/The_Battle_for_Wesnoth"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/battle-wesnoth"
---

The Battle for Wesnoth (La Bataille pour Wesnoth) est un jeu vidéo de stratégie au tour par tour dans un univers médiéval fantastique.
On peut y jouer en solo via des campagnes qui racontent l'histoire du royaume de Wesnoth. Il y a un grand nombre de campagnes créés par des joueurs et mises librement à disposition pour la communauté.
Le jeu a aussi un mode multijoueur via internet ou en local.

