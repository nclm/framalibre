---
nom: "Stellarium"
date_creation: "Vendredi, 23 décembre, 2016 - 04:22"
date_modification: "Mercredi, 12 mai, 2021 - 15:29"
logo:
    src: "images/logo/Stellarium.png"
site_web: "http://stellarium.org/"
plateformes:
    - "Android"
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Stellarium est un logiciel de planétarium pour votre ordinateur."
createurices: "Fabien Chéreau"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "science"
    - "astronomie"
    - "planète"
    - "étoile"
    - "satellite"
    - "télescope"
    - "lunette astronomique"
    - "physique"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Stellarium"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/stellarium"
---

Stellarium est un logiciel de planétarium pour votre ordinateur. Il affiche un ciel réaliste en 3D, comme si vous le regardiez à l’œil nu, aux jumelles ou avec un télescope.
Un catalogue d'étoiles très étendu est disponible par défaut et il est possible de l'enrichir avec des catalogues supplémentaires. Il inclut également des images de planètes, de satellites et d’astéroïdes.
Stellarium est utilisé par certains projecteurs de planétarium. Rentrez vos coordonnées et c'est parti !

