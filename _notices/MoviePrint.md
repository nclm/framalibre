---
nom: "MoviePrint"
date_creation: "Dimanche, 29 décembre, 2019 - 11:27"
date_modification: "Mercredi, 12 mai, 2021 - 15:11"
logo:
    src: "images/logo/MoviePrint.gif"
site_web: "https://movieprint.fakob.com"
plateformes:
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Générez des captures d'écran de clips vidéo."
createurices: "Jakob Schindegger"
alternative_a: ""
licences:
    - "Licence MIT/X11"
tags:
    - "multimédia"
    - "vidéo"
    - "screencast"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/movieprint"
---

Générez des captures d'écran de clips vidéo en quelques clics seulement. Celui-ci fournit une alternative de bureau à la création d’écrans de film, dans une formule personnalisable, à la fois pour l’analyse des images vidéo, ainsi que pour la grille d’images obtenue contenant les vignettes.
Il est possible d'ajouter un ou plusieurs clips vidéo à la fois, par simple glisser-déposer dans la zone prévue à cette effet. Sélectionner la plage de montage souhaitée pour la génération d'impressions, réglez les données, les marges et le tour est joué.
MoviePrint est l'outil idéal pour cataloguer cataloguer sa vaste filmothèque et nous évite des heures de visionnage pour se rappeler du film dont le nom ne nous dit plus rien.
Le programme est codé en javascript avec Electron et node

