---
nom: "Scenari"
date_creation: "Mercredi, 19 avril, 2017 - 22:16"
date_modification: "mercredi, 13 mars, 2024 - 10:55"
logo:
    src: "images/logo/Scenari.png"
site_web: "https://scenari.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Une suite logicielle qui permet la création de documents multisupports (site web, présentation, diapo, pdf, …)"
createurices: "Kelis"
alternative_a: "Microsoft Office, Microsoft Word, Microsoft PowerPoint"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
    - "Licence publique générale limitée GNU (LGPL)"
    - "Licence Publique Mozilla (MPL)"
    - "Licence CECILL (Inria)"
tags:
    - "bureautique"
    - "chaîne éditoriale"
    - "création de site web"
    - "diaporama"
    - "traitement de texte"
    - "exerciseur"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Scenari"
lien_exodus: ""
identifiant_wikidata: "Q3475265"
mis_en_avant: "oui"
redirect_from: "/content/scenari"
---

Scenari est une suite logicielle qui permet de créer des sites web, des présentations, des diaporamas, des pdf, etc, à partir d'une seule source.
Les avantages :

- gain de temps dans la production et la maintenance de ses documents ;
- travail à plusieurs via le réseau ou le cloud ;
- mise en forme est gérée par Scenari ce qui permet de se concentrer sur son rôle d'auteur.

Il existe plusieurs modèles documentaires :

- Projets, présentations : [OptimOffice](https://framalibre.org/notices/optimoffice.html)
- Enseignement parcours linéaire: [Opale](https://framalibre.org/notices/opale.html), [Canoprof](https://framalibre.org/notices/canoprof.html)
- Étude de cas, jeu sérieux: [Topaze](https://framalibre.org/notices/topaze.html)
- Documentation logicielle : [Dokiel](https://framalibre.org/notices/dokiel.html)
- Enrichissement audio/vidéo : [WebMedia](https://framalibre.org/notices/webmedia.html)
- les autres modèles : IdKey, Lexico, Rubis…
