---
nom: "Synkron"
date_creation: "Mardi, 25 avril, 2017 - 17:19"
date_modification: "Mardi, 25 avril, 2017 - 17:19"
logo:
    src: "images/logo/Synkron.png"
site_web: "http://synkron.sourceforge.net/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Sauvegarde de fichiers : interface graphique ergonomique et mémoire des paramétrages"
createurices: "Matúš Tomlein"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "sauvegarde"
    - "synchronisation"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Synkron"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/synkron"
---

Synkron présente l'avantage d'être multiplateforme et multilingue. Il est simple d'utilisation, l'interface étant ergonomique. On peut le considérer comme un pendant graphique de rsync dont il reprend une partie des fonctionnalités. Les fonctions sont présentées sous la forme de différents onglets, un par synchronisation qui reste en mémoire (c'est très pratique).
- synchronisation classique
- "multi-synchronisation" (plusieurs dossiers sources pour une seule destination)
- planificateur de synchronisations

