---
nom: "Speed Dreams"
date_creation: "Jeudi, 29 décembre, 2016 - 19:37"
date_modification: "Mardi, 1 septembre, 2020 - 11:30"
logo:
    src: "images/logo/Speed Dreams.png"
site_web: "http://www.speed-dreams.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Autre"
langues:
    - "English"
description_courte: "Un jeu de course réaliste libre."
createurices: ""
alternative_a: "Forza, Gran Turismo, Project CARS"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "jeu"
    - "course"
    - "jeu de voiture"
lien_wikipedia: "https://en.wikipedia.org/wiki/Speed_Dreams"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/speed-dreams"
---

Speed-Dreams est un simulateur open-source de sport automobile, fork du jeu de course TORCS.
Il a la particularité de se jouer à la souris pour améliorer la précision des déplacements de la voiture, il est également possible d'y jouer au volant.
Le jeu propose de bons graphismes.
Il est à l'heure actuelle difficile d'y jouer sur une distribution GNU/Linux basée sur Debian (comme Ubuntu). La seul solution sur ces distributions est de compiler le jeu manuellement, ou de passer sur un autre système.

