---
nom: "Remarkable"
date_creation: "Jeudi, 5 janvier, 2017 - 23:15"
date_modification: "Jeudi, 10 décembre, 2020 - 11:36"
logo:
    src: "images/logo/Remarkable.png"
site_web: "https://remarkableapp.github.io/"
plateformes:
    - "GNU/Linux"
langues:
    - "English"
description_courte: "Un éditeur markdown efficace, personnalisable et original."
createurices: "Jamie McGowan"
alternative_a: ""
licences:
    - "Licence MIT/X11"
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "bureautique"
    - "markdown"
    - "traitement de texte"
    - "html"
    - "éditeur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/remarkable"
---

Remarkable est un éditeur Markdown utilisant une syntaxe à la « saveur » Github. Il propose une prévisualisation HTML en temps réel et permet des exports PDF et HTML. Le style de l'affichage est personnalisable (via CSS). Outre la coloration syntaxique, il supporte aussi MathJax pour afficher des éléments mathématiques. Écrit en Python, ce logiciel dispose d'une interface originale et agréable.

