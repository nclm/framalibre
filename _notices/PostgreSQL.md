---
nom: "PostgreSQL"
date_creation: "Mercredi, 27 mars, 2019 - 03:01"
date_modification: "Vendredi, 29 mars, 2019 - 02:38"
logo:
    src: "images/logo/PostgreSQL.png"
site_web: "https://www.postgresql.org/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Windows Mobile"
    - "Autre"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Le plus complet des SGBDRO en open-source et le seul capable de rivaliser avec les plus grands"
createurices: "Communauté mondiale des développeurs PostgreSQL"
alternative_a: "MariaDB, Oracle Database, Oracle Enterprise Manager Database Management, Microsoft SQL Server"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "système"
    - "base de données"
    - "gestionnaire de base de données"
    - "manipulation de données"
    - "sig"
    - "géographie"
    - "sql"
lien_wikipedia: "https://fr.wikipedia.org/wiki/PostgreSQL"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/postgresql-0"
---

PostgreSQL est le plus complet des systèmes de gestion de base de données relationnelle-objet (SGBDRO) sous licence open-source.
De façon simplifiée, un SGBDRO c'est une structure qui permet de mémoriser non seulement des données mais également des objets.
PostgreSQL supporte en autre les schémas multiples, les types, les domaines, les séquences, les MTQ, les opérateurs, etc... qui ne sont pas supportées dans les SGBDs plus simples comme MySQL ou MariaDB.
PostgreSQL procède comme extension géographique le module PostGIS qui est basée sur GDAL (Geospatial Data Abstraction Library) de l' OSGeo (Fondation Open Source Geospatial)
Très bonne stabilité et garantie des propriétés ACID (Atomicité, cohérence, Isolation, Durabilité) et du MVCC (Multiversion Concurrency Control)
Et grâce à sa licence libérale, PostgreSQL peut être utilisé, modifié et distribué librement, quel que soit le but visé : privé, commercial ou académique.
Source publié sur   https://git.postgresql.org

