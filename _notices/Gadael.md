---
nom: "Gadael"
date_creation: "Lundi, 24 juillet, 2017 - 16:26"
date_modification: "Mercredi, 18 avril, 2018 - 15:40"
logo:
    src: "images/logo/Gadael.png"
site_web: "http://www.gadael.org"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Logiciel de gestion des congés nodejs/angularjs"
createurices: "Paul de Rosanbo"
alternative_a: "Sage"
licences:
    - "Licence MIT/X11"
tags:
    - "métiers"
    - "congés"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/gadael"
---

Ce logiciel permet de gérer les congés dans une moyenne ou une grosse entreprise, il permet de déposer des congés qui serons gérés dans un circuit d'approbation.
Les droits spéciaux peuvent être gérés comme le calcul automatique du nombre de RTT, les droits d'ancienneté, la diminution des RTT en fonction des arrêts maladie, les jours de fractionnement.
Les plannings de rythme de travail sont géré avec des fichiers ICS un utilisant le format ouvert icalendar.

