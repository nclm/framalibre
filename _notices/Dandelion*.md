---
nom: "Dandelion*"
date_creation: "Mercredi, 4 janvier, 2017 - 15:29"
date_modification: "Mercredi, 4 janvier, 2017 - 22:04"
logo:
    src: "images/logo/Dandelion*.png"
site_web: "https://github.com/Diaspora-for-Android/dandelion/blob/HEAD/README.md"
plateformes:
    - "Android"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Client Android pour le réseau social libre décentralisé Diaspora*."
createurices: ""
alternative_a: "Facebook, Twitter"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "diaspora"
    - "vie privée"
    - "décentralisation"
    - "android"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/dandelion"
---

Client Android pour le réseau social libre décentralisé Diaspora
Il est utilisable derrière le réseau TOR avec l'aide de Orbot.
Dandelion n'est disponible en téléchargement que sur F-Droid.
(non disponible sur Google Play Store)

