---
nom: "MySQL Workbench"
date_creation: "Vendredi, 21 avril, 2017 - 19:36"
date_modification: "Vendredi, 21 avril, 2017 - 20:21"
logo:
    src: "images/logo/MySQL Workbench.png"
site_web: "https://www.mysql.com/products/workbench/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "English"
description_courte: "Logiciel de développement, création et gestion de bases de données particulièrement complet et performant."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "gestionnaire de base de données"
    - "base de données"
    - "mysql"
lien_wikipedia: "https://fr.wikipedia.org/wiki/MySQL_Workbench"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/mysql-workbench"
---

MySQL Workbench permet de concevoir ses bases de données avec toute une panoplie d'outils et de représentations graphiques (un genre d'AnalyseSI survitaminé).
On peut connecter ses bases de données et les gérer / administrer  avec ce même outil.
Comme son nom l'indique il permet de travailler avec les bases de données MySQL mais de nombreux autres formats sont également supportés ou connectables.

