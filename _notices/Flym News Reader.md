---
nom: "Flym News Reader"
date_creation: "Mardi, 18 avril, 2017 - 18:23"
date_modification: "Mercredi, 12 mai, 2021 - 16:40"
logo:
    src: "images/logo/Flym News Reader.png"
site_web: "https://play.google.com/store/apps/details?id=net.fred.feedex&hl=en"
plateformes:
    - "Android"
    - "le web"
langues:
    - "English"
description_courte: "Agrégateur de flux rss/atom sur Android"
createurices: "Frédéric Julian"
alternative_a: "Feedly"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "lecteur de flux rss"
    - "atom"
    - "agrégateur"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/flym-news-reader"
---

Flym est une application fonctionnant sous Android qui permet de lire des flux rss/atom. Il permet de suivre facilement un flux d'information (par exemple les derniers articles de framasoft) sans avoir à se rendre manuellement sur le site avec un navigateur web.
L'application permet de:
- lire des articles et les images qu'ils contiennent hors-ligne
- faire une recherche dans ses articles
- filtrer par mot-clé
- créer des widgets personnalisables

