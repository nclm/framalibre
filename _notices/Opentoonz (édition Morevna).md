---
nom: "Opentoonz (édition Morevna)"
date_creation: "Vendredi, 4 mai, 2018 - 20:45"
date_modification: "Mercredi, 18 octobre, 2023 - 11:19"
logo:
    src: "images/logo/Opentoonz (édition Morevna).png"
site_web: "https://morevnaproject.org/opentoonz/?lang=fr"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Autres langues"
description_courte: "Logiciel professionnel d'animation 2D."
createurices: ""
alternative_a: "Toonz"
licences:
    - "Berkeley Software Distribution License (BSD)"
tags:
    - "création"
    - "animation"
    - "dessin animé"
    - "éditeur"
    - "créativité"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/opentoonz-%C3%A9dition-morevna"
---

Logiciel pro d'animation 2D, basé sur celui du studio japonais Ghibli.
Cette édition Morevna est celle du grand projet autour de l'animation 2D, groupe international, collaboratif, et qui met en open-source 3 autes logiciels (Synfig, Renderchan, Papagayo-NG) et de nombreux projets réalisés.
Par rapport à l'édition Dwango, cette édition propose les différence suivantes :
* intégration avec le moteur des brosses MyPaint
* ligne de temps horizontale
* sélecteur avancé de couleur
* version Windows 32 bits
* version Linux (éxécutables)
Autre lien complémentaire : sourceforge.net/projects/opentoonz-morevna-edition.
License : modified BSD license.

