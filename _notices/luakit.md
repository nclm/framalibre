---
nom: "luakit"
date_creation: "Lundi, 4 février, 2019 - 21:58"
date_modification: "Mercredi, 12 mai, 2021 - 14:52"
logo:
    src: "images/logo/luakit.png"
site_web: "https://luakit.github.io/"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Windows"
langues:
    - "Autres langues"
description_courte: "Luakit, la navigation internet au bout des doigts !
Une alternative incroyablement sobre en ressources."
createurices: "Aidan Holm, Mason Larobina"
alternative_a: "Microsoft Edge, Opera, Google Chrome, Chrome, Internet Explorer"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "navigateur web"
    - "commande clavier"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/luakit"
---

Luakit est un navigateur internet très configurable. Il est basé sur le moteur WebKit et utilise les bibliothèques GTK+.
Il est extrêmement rapide et peu gourmand en ressources. Il est également très configurable, acceptant pour en étendre l'usage des scripts écrits à l'aide du langage Lua (d'où son nom).
Il peut se piloter entièrement au clavier (taper ":binds" pour la liste des raccourcis).
Luakit est destiné à l'origine aux "power users" ou utilisateurs avancés qui veulent un outils extrêmement configurable.

