---
nom: "SolveSpace"
date_creation: "Vendredi, 22 septembre, 2017 - 16:32"
date_modification: "jeudi, 11 janvier, 2024 - 01:04"
logo:
    src: "images/logo/SolveSpace.png"
site_web: "http://solvespace.com/index.pl"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
description_courte: "Logiciel de dessin technique à contraintes."
createurices: "Jonathan Westhues"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "création"
    - "cad"
    - "dao"
    - "dessin technique"
    - "dessin"
lien_wikipedia: "https://en.wikipedia.org/wiki/SolveSpace"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/solvespace"
---

Ce logiciel de dessin technique très ergonomique.
Il suffit de suivre le premier tutoriel (tout en anglais actuellement) pour être autonome et créer ses propres montages.
Il permet notamment de créer des réalisations à liaisons mécaniques sur lesquels on place des contraintes. On peut ensuite accrocher des objets 3D à ce squelette pour un rendu plus représentatif, mais la composition simple, rapide et surtout intuitive du squelette remplit une fonction qu'on trouve très difficilement dans ce type de logiciels.


