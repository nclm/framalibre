---
nom: "GNU Emacs"
date_creation: "Mardi, 27 décembre, 2016 - 12:17"
date_modification: "Mardi, 3 janvier, 2017 - 19:59"
logo:
    src: "images/logo/GNU Emacs.png"
site_web: "https://www.gnu.org/software/emacs/"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "Autre"
    - "BSD"
langues:
    - "English"
description_courte: "Emacs est un des éditeurs de texte libre extensible et toujours très populaire depuis sa naissance en 1976."
createurices: "Richard Stallman"
alternative_a: "Notepad, UltraEdit, Sublime Text, PyCharm"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "développement"
    - "traitement de texte"
    - "texte"
    - "éditeur"
    - "environnement de développement"
lien_wikipedia: "https://fr.wikipedia.org/wiki/GNU_Emacs"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/gnu-emacs"
---

Emacs est l'un des éditeurs de texte historiques du logiciel libre, créé par Richard Stallman, le « papa » du logiciel libre. Il est extrêmement personnalisable, extensible et peu faire à peu près tout ce que vous voulez (y compris des choses qui n'ont rien à voir avec un éditeur de texte classique, comme gérer ses mails, naviguer sur Internet, lire des documents PDF,  jouer à Tetris, etc.). Il est très populaire chez les développeurs informatiques, mais il peut tout aussi bien être utilisé pour écrire n'importe quoi.
Lorsqu'il est maîtrisé, il permet d'être très efficace et performant, mais attention, il y a un saut technique à faire. Le principal frein à son utilisation vient des raccourcis clavier par défaut qui ne sont pas les mêmes que sur la plupart des autres logiciels (le Ctrl-C pour copier, par exemple, demande d'avoir coché l'option). Mais si vous avez la patience d'apprendre à vous en servir, foncez, une fois maîtrisé, on ne regrette pas !

