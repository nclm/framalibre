---
nom: "Brasero"
date_creation: "Samedi, 24 décembre, 2016 - 13:13"
date_modification: "Samedi, 17 juin, 2017 - 06:30"
logo:
    src: "images/logo/Brasero.png"
site_web: "https://wiki.gnome.org/Apps/Brasero"
plateformes:
    - "GNU/Linux"
langues:
    - "Français"
    - "Autres langues"
description_courte: "Chauffez votre graveur de CD/DVD avec un logiciel rapide simple à l'emploi."
createurices: "Philippe Rouquier, Luis Medinas, Joshua Lock"
alternative_a: "Nero"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "multimédia"
    - "gravure"
    - "dvd"
    - "cd-rom"
    - "iso"
    - "copie"
    - "stockage"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Brasero_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/brasero"
---

Brasero est un logiciel de gravure pour CD/DVD pour l'environnement de bureau Gnome (mais il fonctionne très bien sur d'autres bureaux). L'interface graphique est simple et les assistants de projet permettent de configurer un processus de gravure en un temps record. Il est de même possible d'enregistrer les tâches. Brasero peut graver des fichiers stockés à distance (greffon de téléchargement), peut créer des images et les stocker sur un disque dur, permet l'édition CD-Text, détecte le matériel, copie des supports à la volée, etc.

