---
nom: "Superset Visual Database"
date_creation: "Dimanche, 9 juillet, 2023 - 22:57"
date_modification: "Dimanche, 9 juillet, 2023 - 23:02"
logo:
    src: "images/logo/Superset Visual Database.png"
site_web: "https://superset.apache.org"
plateformes:
    - "GNU/Linux"
    - "Mac OS X"
    - "Windows"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Superset facilite l'exploration de nos données,"
createurices: ""
alternative_a: "Power BI, Tableau, Qlik, MicroStrategy"
licences:
    - "Licence Apache (Apache)"
tags:

lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/superset-visual-database"
---

Superset est rapide, léger, intuitif et regorge d'options qui permettent aux utilisateurs de toutes compétences d'explorer et de visualiser facilement leurs données, des simples graphiques linéaires aux graphiques géospatiaux très détaillés.
Superset facilite l'exploration de vos données, à l'aide de notre simple générateur de visualisations sans code ou de notre IDE SQL à la pointe de la technologie.
S'intègre aux bases de données modernes
Superset peut se connecter à toutes les bases de données basées sur SQL, y compris les bases de données et les moteurs natifs du cloud modernes à l'échelle du pétaoctet.
Architecture moderne
Superset est léger et hautement évolutif, tirant parti de la puissance de votre infrastructure de données existante sans nécessiter une autre couche d'ingestion.
Visualisations et tableaux de bord riches
Superset est livré avec plus de 40 types de visualisation préinstallés. Notre architecture de plug-in facilite la création d

