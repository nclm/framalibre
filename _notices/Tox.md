---
nom: "Tox"
date_creation: "Lundi, 13 mars, 2017 - 21:26"
date_modification: "Mercredi, 12 mai, 2021 - 16:19"
logo:
    src: "images/logo/Tox.png"
site_web: "https://tox.github.io"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "Apple iOS"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Tox est un logiciel de messagerie, voip et visioconférence chiffré utilisant partiellement la technologie p2p."
createurices: ""
alternative_a: "Skype"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "p2p"
    - "voip"
    - "chat"
    - "visioconférence"
    - "messagerie instantanée"
    - "partage de fichiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Tox_(logiciel)"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/tox"
---

Messagerie instantanée, voix sur ip, visioconférence, échange de fichier, Tox est un logiciel complet. Il fonctionne en pair à pair (p2p). C'est un logiciel partiellement décentralisé: la partie message (100% chiffrée) et la partie id le sont. Cependant l'authentification est faite par un serveur central, le stockage des messages hors ligne et l'utilisation des relais TCP n'utilisent pas une approche décentralisée. Toutefois il est possible de désactiver l'option de stockage des messages hors ligne. Il existe plusieurs clients disponibles fournissant différentes interfaces pour chaque OS.
Le projet a souffert de plusieurs divisions, notamment par la scission en deux versions différentes du projet initial. Suite à de nombreuses péripéties et désaccords Tox.chat a récupéré la marque Tox stipulant que le projet Tox.im hébergeait certaines personnes mal intentionnées écrivant des botnets pour le compte de la NSA. Cette histoire fit énormément de mal au projet encore jeune qu'était Tox.

