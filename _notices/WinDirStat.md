---
nom: "WinDirStat"
date_creation: "Vendredi, 16 octobre, 2020 - 11:18"
date_modification: "Vendredi, 16 octobre, 2020 - 11:18"
logo:
    src: "images/logo/WinDirStat.png"
site_web: "https://windirstat.net"
plateformes:
    - "Windows"
langues:
    - "Français"
    - "English"
    - "Español"
    - "Autres langues"
description_courte: "Logiciel d'espace disque utilisé par dossier, ou disque."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "système"
lien_wikipedia: "https://fr.wikipedia.org/wiki/WinDirStat"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/windirstat"
---

Logiciel de calcul d'espace disque. Analyse sur la totalité du disque ou par dossier. Permet l'effacement complet de dossiers. Propose une représentation visuel de l'espace utilisé, par dossier ou par type de fichiers.

