---
nom: "Kresus"
date_creation: "Samedi, 21 janvier, 2017 - 15:22"
date_modification: "Mardi, 28 août, 2018 - 18:12"
logo:
    src: "images/logo/Kresus.png"
site_web: "https://kresus.org"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Kresus est un gestionnaire de finances personnelles libre et auto-hébergeable."
createurices: "Benjamin Bouvier, ZeHiro, Nicofrand, Phyks"
alternative_a: "linxo, mint, budgea, Microsoft Money, Bankin"
licences:
    - "Licence MIT/X11"
tags:
    - "finances"
    - "budget"
    - "comptabilité"
    - "gestion"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "oui"
redirect_from: "/content/kresus"
---

Kresus est un gestionnaire de finances personnelles qui vous permet de suivre l'état de vos finances, en important tous les comptes de toutes vos banques à un seul endroit. Des alertes emails sur montant d'une transaction ou sur solde peuvent être configurées par vos soins et vous prévenir en cas d'événements importants sur vos comptes. La possibilité de créer des catégories et ensuite d'attribuer ces catégories à vos comptes vous donne les réponses à des questions telles que « Quel est mon plus gros poste de dépenses ce mois-ci ? » ou encore « Comment se comparent mes rentrées d'argent au cours des six derniers mois ? »

