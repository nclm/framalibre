---
nom: "Volumio"
date_creation: "Mardi, 25 avril, 2017 - 14:25"
date_modification: "Samedi, 17 juin, 2017 - 09:43"
logo:
    src: "images/logo/Volumio.png"
site_web: "https://volumio.org"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Plate-forme d'écoute de musique accessible depuis vos ordinateurs et vos tablettes / smartphones."
createurices: "Michelangelo Guarise"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "lecteur audio"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/volumio"
---

Volumio est une plate-forme d'écoute de musique consultable depuis tous les systèmes d'exploitation.
Personnellement j'en ai eu marre d'écouter des musiques dégradées par la compression mp3 :
J'ai donc décidé de reprendre mes CD et d'en emprunter dans la famille pour me créer une base en .flac ;
J'ai placé ces albums sur un NAS maison piloté par un Raspberry Pi 1 ;
J'ai installé Volumio sur un RasPi 2 et la distribution a immédiatement démarré ;
J'ai connecté le RasPi 2 en numérique (USB) à un ampli numérique relié à de bonnes enceintes (pas besoin de DAC) ; 
Je peux commander mon VOLUMIO depuis tout ordinateur ou smartphone connecté au réseau ;
NB : Volumio trouve directement les musiques sur votre NAS et lit toute une liste de radios sur internet.

