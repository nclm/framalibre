---
nom: "FullSync"
date_creation: "Vendredi, 6 janvier, 2017 - 13:33"
date_modification: "Lundi, 9 janvier, 2017 - 11:56"
logo:
    src: "images/logo/FullSync.png"
site_web: "https://fullsync.sourceforge.io/index.php"
plateformes:
    - "GNU/Linux"
    - "Windows"
langues:
    - "Français"
    - "English"
description_courte: "Logiciel de gestion de sauvegarde de données, de synchronisation de fichiers, et de restauration."
createurices: ""
alternative_a: "SyncBack"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "internet"
    - "synchronisation"
    - "sauvegarde"
    - "manipulation de fichier"
    - "client ftp"
    - "transfert de fichiers"
lien_wikipedia: "https://fr.wikipedia.org/wiki/FullSync"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/fullsync"
---

FullSync est un logiciel qui permet de sauvegarder, synchroniser ou restaurer des fichiers de façon manuelle ou automatique. Il gère les protocoles FTP, SFTP et SMB.
Cet outil convient aussi bien aux novices qu'aux experts pour la gestion des fichiers, et permet à l'utilisateur de gérer les actions à effectuer (Que faire si deux fichiers sont identiques ? Qui écrase qui ? En fonction du poids ou de la date ?).

