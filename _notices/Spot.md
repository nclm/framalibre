---
nom: "Spot"
date_creation: "Vendredi, 24 septembre, 2021 - 23:58"
date_modification: "Lundi, 27 septembre, 2021 - 20:08"
logo:
    src: "images/logo/Spot.png"
site_web: "https://spot.ecloud.global"
plateformes:
    - "GNU/Linux"
    - "BSD"
    - "Mac OS X"
    - "Windows"
    - "Android"
    - "FirefoxOS"
    - "Windows Mobile"
    - "Apple iOS"
    - "Autre"
langues:
    - "Autres langues"
description_courte: "Un moteur de recherche efficace et ergonomique."
createurices: ""
alternative_a: "Google Search, Bing, Yahoo"
licences:
    - "Licence Publique Générale Affero (AGPL)"
tags:
    - "internet"
    - "métamoteur de recherche"
    - "web"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/spot"
---

Spot est un méta-moteur de recherche basé sur Searx développé par la e.foundation, dans le but de remplacer Google Search.
Bien qu'il arrive qu'il y ait des erreurs et que la page ne charge pas, il permet au quotidien d'effectuer des recherches paisiblement. Il est au départ le moteur de recherche par défaut sur les téléphones tournant sous /e/OS, mais il peut bien évidemment être changé tout comme il peut être utilisé avec n'importe quel navigateur web.

