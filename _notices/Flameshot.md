---
nom: "Flameshot"
date_creation: "mardi, 13 février, 2024 - 22:53"
date_modification: "dimanche, 3 mars, 2024 - 14:50"
logo:
    src: "images/logo/Flameshot.svg"
site_web: "https://flameshot.org/"
plateformes:
    - "Mac OS X"
    - "Windows"
    - "GNU/Linux"
langues:
    - "Français"
    - "English"
description_courte: "Flameshot est un utilitaire de capture d'écran riche en fonctionnalités !"
createurices: "Flameshot.org"
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "capture d'écran"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: "Q66978303"
mis_en_avant: "non"

---

**Flameshot** est un outil de capture d'écran qui peut se lancer depuis la barre des tâches.

Une fenêtre redimensionnable s'affiche alors, et il est possible d'y ajouter dynamiquement toute sorte d'annotations (cercles numérotés, formes géométriques simples, texte, etc.) de différentes couleurs, de différentes épaisseurs...

On peut ensuite copier l'image dans le presse-papier, la partager ou bien l'enregistrer.
