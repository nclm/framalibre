---
nom: "Cyca"
date_creation: "Lundi, 12 octobre, 2020 - 12:39"
date_modification: "Lundi, 23 août, 2021 - 12:42"
logo:
    src: "images/logo/Cyca.png"
site_web: "https://git.athaliasoft.com/richard/cyca"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Français"
    - "English"
description_courte: "Gestionnaire de favoris et de flux basé sur le web."
createurices: "Richard Dern"
alternative_a: "Feedly"
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "marque-pages"
    - "flux"
    - "atom"
    - "lecteur de flux rss"
lien_wikipedia: ""
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/cyca"
---

Cyca est un gestionnaire de favoris et de flux (atom et RSS) basé sur le web, non-intrusif, pensé pour le bureau, facile à installer avec docker et facile à utiliser au quotidien.
Il permet de gérer, via une interface commune, un grand nombre de favoris et de flux. La découverte automatique des flux permet d'accéder à toute l'actualité de vos sites préférés.

