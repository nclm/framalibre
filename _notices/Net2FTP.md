---
nom: "Net2FTP"
date_creation: "Mercredi, 4 octobre, 2017 - 00:35"
date_modification: "Mardi, 20 novembre, 2018 - 22:41"
logo:
    src: "images/logo/Net2FTP.png"
site_web: "https://net2ftp.com/"
plateformes:
    - "GNU/Linux"
    - "le web"
langues:
    - "Autres langues"
description_courte: "Net2FTP, un outil particulièrement utile pour manipuler les fichiers de votre site web ou de votre serveur."
createurices: ""
alternative_a: ""
licences:
    - "Licence Publique Générale GNU (GNU GPL)"
tags:
    - "transfert de fichiers"
    - "ftp"
lien_wikipedia: "https://en.wikipedia.org/wiki/Net2ftp"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/net2ftp"
---

Net2FTP est un client&serveur ftp (FTP : Protocole de Transfert de Fichiers) qui permet de gérer facilement et intuitivement les fichiers de votre site web ou d'un de vos serveurs distants.
Le logiciel peut être installé simplement sur votre plateforme personnelle en tant qu'application web. Il fonctionne avec un serveur Web (Apache, ...) et nécessite une version peu évoluée de PHP. En option, il peut stocker les logs dans une base type MySQL/MARIADB.
Il est possible aussi d'utiliser la version en ligne disponible sur le site de l'éditeur (A ce niveau, c'est une question de confiance ...)
De façon générale, Net2FTP permet d'effectuer les opérations courantes sur le système de fichiers, de modifier les droits, d'installer des applications web, de compresser ou décompresser des archives, de télécharger, de déterminer la taille des fichiers d'une sélection ou de rechercher un mot dans un ensemble de fichiers.
Le site n'est plus accessible aux 27 pays de l'UE (Grande-Bretagne inclut).

