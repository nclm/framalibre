---
nom: "Debian GNU/Linux"
date_creation: "Lundi, 26 décembre, 2016 - 21:50"
date_modification: "Lundi, 10 mai, 2021 - 14:06"
logo:
    src: "images/logo/Debian GNU-Linux.png"
site_web: "https://www.debian.org/index.fr.html"
plateformes:
    - "GNU/Linux"
langues:
    - "Autres langues"
description_courte: "Une distribution GNU/Linux historique, non commerciale et démocratique."
createurices: ""
alternative_a: "Microsoft Windows, MacOS"
licences:
    - "Multiples licences"
tags:
    - "système"
    - "système d'exploitation (os)"
    - "distribution gnu/linux"
    - "linux"
    - "debian"
lien_wikipedia: "https://fr.wikipedia.org/wiki/Debian"
lien_exodus: ""
identifiant_wikidata: ""
mis_en_avant: "non"
redirect_from: "/content/debian-gnulinux"
---

Debian GNU/Linux est un système d'exploitation créé par Debian, organisation communautaire dont l'objet est le développement d'OS basé sur des logiciels libres. Cette distribution sert de base à de nombreuses autres, incluant ou non des paquets propriétaires, au nombre desquelles on peut recenser Ubuntu, Linux Mint, Knoppix...
Outre son modèle coopératif de grande renommée, l'objectif de Debian GNU/Linux est de proposer une solution d'OS polyvalent, pour ordinateur de bureau ou serveur. La distribution présente ses différentes versions sous trois branches : stable (les seules mises à jour sont des correctifs de sécurité), testing (mises à jour de sécurité + certains paquets suffisamment stables), unstable (autrement appelée Sid, en constante évolution).
Un .exe pour installer la dernière version depuis Windows est disponible ici

