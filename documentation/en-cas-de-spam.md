# En cas de spam - "Gros bouton rouge"

## Arrêter le serveur de contribution en urgence

En cas d'urgence, il est possible d'arrêter le serveur de contribution en allant modifier le fichier [_config.yml](/_config.yml)
Il suffit :
1) d'enlever le `#` devant la ligne `#contribution_anonyme: false` 
2) de rajouter un `#` devant la ligne `contribution_anonyme: true`

Seule une personne ayant les droits en écriture à ce repository framagit peut faire cette manipulation

(faire l'inverse pour les ré-activer)

Normalement, les contributions anonymes se désactiveront dans un délai inférieur à une minute

Les contributions ne seront possibles qu'uniquement via framagit, directement sur ce repository en modifiant les markdown/Yaml à la main et par les personnes ayant un compte sur framagit


## Autres solutions

D'autres solutions ont été discutées dans une issue : https://framagit.org/framasoft/framalibre/-/issues/48
